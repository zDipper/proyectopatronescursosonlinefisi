﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaEntidad;

namespace CapaDeDatos
{
    public class UsuarioDA
    {
        ContextDA context = new ContextDA();

        public UsuarioBE ObtenerUsuarioCuenta(string correo, string password)
        {
            try
            {
                return context.DbUsuario.Where(v => v.CorreoUsuario.ToLower() == correo.ToLower()).Where(v => v.ContrasenaUsuario == password).Single();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new UsuarioBE();
            }
            
        }

        public bool ExistEmail(string correo)
        {
            try
            {
                UsuarioBE usuarioBE = context.DbUsuario.Where(v => v.CorreoUsuario.ToLower() == correo.ToLower()).Single();
                if (usuarioBE.CorreoUsuario.ToLower().Equals(correo.ToLower()))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
                return false;
            }
        }
    }
}
