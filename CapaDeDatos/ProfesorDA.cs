﻿using CapaEntidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDeDatos
{
    public class ProfesorDA
    {
        ContextDA context = new ContextDA();

        public ProfesorBE ObtenerProfesorDni(string dni)
        {

            try
            {
                return context.DbProfesor.Where(v => v.dniProfesor == dni).Single();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new ProfesorBE();
            }
        }
    }
}
