﻿using CapaEntidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDeDatos
{
    public class AlumnoDA
    {
        ContextDA context = new ContextDA();

        public AlumnoBE ObtenerAlumnoDni(string dni)
        {
            try
            {
                return context.DbAlumno.Where(v => v.dniAlumno == dni).Single();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new AlumnoBE();
            }
        }
        public string ObtenerUltimoCodigoAlumno()
        {
            try
            {
                return context.DbAlumno.ToList().OrderBy(v => v.codAlumno).Last().codAlumno;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return "50000";
            }
        }
    }
}
