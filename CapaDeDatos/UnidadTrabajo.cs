﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaEntidad;

namespace CapaDeDatos
{
    public class UnidadTrabajo:IDisposable
{
        // creamos los repositorios para poder usar los metodos de RepositorioGeneral
        private ContextDA context = new ContextDA();
        private RepositorioGeneral<UsuarioBE> usuarioRepositorio;
        private RepositorioGeneral<AlumnoBE> alumnoRepositorio;
        private RepositorioGeneral<ProfesorBE> profesorRepositorio;
        private RepositorioGeneral<AdministradorBE> administradorRepositorio;
        private RepositorioGeneral<CursoBE> cursoRepositorio;
        private RepositorioGeneral<AlumnoCursoBE> alumnoCursoRepositorio;
        private RepositorioGeneral<CapituloBE> capituloRepositorio;

        // hacemos uso de los repositorios
        public RepositorioGeneral<UsuarioBE> UsuarioRepositorio
        {
            get
            {
                return this.usuarioRepositorio ?? new RepositorioGeneral<UsuarioBE>(context);
            }
        }
        public RepositorioGeneral<AlumnoBE> AlumnoRepositorio
        {
            get
            {
                return this.alumnoRepositorio ?? new RepositorioGeneral<AlumnoBE>(context);
            }
        }
        public RepositorioGeneral<ProfesorBE> ProfesorRepositorio
        {
            get
            {
                return this.profesorRepositorio ?? new RepositorioGeneral<ProfesorBE>(context);
            }
        }
        public RepositorioGeneral<AdministradorBE> AdministradorRepositorio
        {
            get
            {
                return this.administradorRepositorio ?? new RepositorioGeneral<AdministradorBE>(context);
            }
        }
        public RepositorioGeneral<CursoBE> CursoRepositorio
        {
            get
            {
                return this.cursoRepositorio ?? new RepositorioGeneral<CursoBE>(context);
            }
        }
        public RepositorioGeneral<AlumnoCursoBE> AlumnoCursoRepositorio
        {
            get
            {
                return this.alumnoCursoRepositorio ?? new RepositorioGeneral<AlumnoCursoBE>(context);
            }
        }
        public RepositorioGeneral<CapituloBE> CapituloRepositorio
        {
            get
            {
                return this.capituloRepositorio ?? new RepositorioGeneral<CapituloBE>(context);
            }
        }

        // para poder hacer el guardado en caso de actualizacion o agregar
        public void Save()
        {
            context.SaveChanges();
        }

        // con estos metodos hacemos que se liberen los recursos que no estamo usando
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
