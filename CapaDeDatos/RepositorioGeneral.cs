﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using CapaEntidad;

namespace CapaDeDatos
{
    // aca se crean los metodos que son identicos para todas entidades
    public class RepositorioGeneral<TEntity> where TEntity : class
    {
        internal ContextDA context;
        internal DbSet<TEntity> dbSet;

        public RepositorioGeneral(ContextDA context)
        {
            this.context = context;
            this.dbSet = context.Set<TEntity>();
        }

        // lista todo lo que tenga de entidad
        public virtual List<TEntity> Get()
        {
            return dbSet.ToList();
        }

        // busca por el identificador
        public virtual TEntity GetByID(object id)
        {
            return dbSet.Find(id);
        }

        // inserta una entidad
        public virtual bool Insert(TEntity entity)
        {
            dbSet.Add(entity);
            return true;
        }

        // borra la entidad por un id
        public virtual bool Delete(object id)
        {
            TEntity entityToDelete = dbSet.Find(id);
            Delete(entityToDelete);
            return true;
        }

        // se usa en la el borrado por id
        public virtual void Delete(TEntity entityToDelete)
        {
            if (context.Entry(entityToDelete).State == EntityState.Detached)
            {
                dbSet.Attach(entityToDelete);
            }
            dbSet.Remove(entityToDelete);
        }

        //  se hace una actualzacion de una entidad
        public virtual bool Update(TEntity entityToUpdate)
        {
            dbSet.Attach(entityToUpdate);
            context.Entry(entityToUpdate).State = EntityState.Modified;
            return true;
        }
    }
}