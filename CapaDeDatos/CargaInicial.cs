﻿using CapaEntidad;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDeDatos
{
    public class CargaInicial : DropCreateDatabaseIfModelChanges<ContextDA>
    {
        protected override void Seed(ContextDA context)
        {
            try
            {
                // llenado de la carga inicial
                List<UsuarioBE> Usuarios = new List<UsuarioBE>()
                {
                    //administradores
                    new UsuarioBE() { DniUsuario = "71755575", NombresUsuario = "Eisten", ApellidosUsuario = "Flores Sanchez", CorreoUsuario = "eisten.flores@unmsm.edu.pe", ContrasenaUsuario = "123456" },
                    // profesores
                    new UsuarioBE() { DniUsuario = "71000575", NombresUsuario = "Adela", ApellidosUsuario = "Pamanes Fierro", CorreoUsuario = "adela.pamanes@gmail.com", ContrasenaUsuario = "654321" },
                    new UsuarioBE() { DniUsuario = "46110945", NombresUsuario = "Andres", ApellidosUsuario = "Angel Alcivar", CorreoUsuario = "andres.angel@gmail.com", ContrasenaUsuario = "654321"},
                    new UsuarioBE() { DniUsuario = "66732448", NombresUsuario = "Javier", ApellidosUsuario = "Esteban Liñan", CorreoUsuario = "javier.esteban@gmail.com", ContrasenaUsuario = "654321"},
                    new UsuarioBE() { DniUsuario = "65867475", NombresUsuario = "Antonia", ApellidosUsuario = "Angeles Poblador", CorreoUsuario = "antonia.angeles@gmail.com", ContrasenaUsuario = "654321"},
                    //alumnos
                    new UsuarioBE() { DniUsuario = "30616990", NombresUsuario = "Casimiro", ApellidosUsuario = "Lunar Iriusta", CorreoUsuario = "casimiro.lunar@gmail.com", ContrasenaUsuario = "123456"},
                    new UsuarioBE() { DniUsuario = "95367004", NombresUsuario = "Carmen", ApellidosUsuario = "Concepcion Llobera", CorreoUsuario = "carmen.concepcion@gmail.com", ContrasenaUsuario = "123456" },
                    new UsuarioBE() { DniUsuario = "37348694", NombresUsuario = "Eva", ApellidosUsuario = "Maria Bernaus", CorreoUsuario = "eva.maria@gmail.com", ContrasenaUsuario = "123456"},
                    new UsuarioBE() { DniUsuario = "36284584", NombresUsuario = "Tomas", ApellidosUsuario = "David Peribañez", CorreoUsuario = "tomas.david@gmail.com", ContrasenaUsuario = "123456"},
                    new UsuarioBE() { DniUsuario = "70652319", NombresUsuario = "Marco", ApellidosUsuario = "Del Rey", CorreoUsuario = "Marco.Rey@gmail.com", ContrasenaUsuario = "123456"},
                    new UsuarioBE() { DniUsuario = "56786945", NombresUsuario = "Jose", ApellidosUsuario = "Sebastian Taboas", CorreoUsuario = "Jose.Sebastian@gmail.com", ContrasenaUsuario = "123456" },
                    new UsuarioBE() { DniUsuario = "58098730", NombresUsuario = "Margarita Rosario", ApellidosUsuario = "Farras", CorreoUsuario = "Margarita.Farras@gmail.com", ContrasenaUsuario = "123456"},
                    new UsuarioBE() { DniUsuario = "77772638", NombresUsuario = "Onelia", ApellidosUsuario = "Alegria", CorreoUsuario = "Onelia.Alegria@gmail.com", ContrasenaUsuario = "123456"},
                    new UsuarioBE() { DniUsuario = "65220218", NombresUsuario = "Nuria", ApellidosUsuario = "Mar Filgueiras", CorreoUsuario = "Nuria.Mar@gmail.com", ContrasenaUsuario = "123456" },
                    };

                List<AlumnoBE> Alumnos = new List<AlumnoBE>()
                {
                    new AlumnoBE() {codAlumno="100000", dniAlumno="30616990"},
                    new AlumnoBE() {codAlumno="100001", dniAlumno="95367004"},
                    new AlumnoBE() {codAlumno="100002", dniAlumno="37348694"},
                    new AlumnoBE() {codAlumno="100003", dniAlumno="36284584"},
                    new AlumnoBE() {codAlumno="100004", dniAlumno="70652319"},
                    new AlumnoBE() {codAlumno="100005", dniAlumno="56786945"},
                    new AlumnoBE() {codAlumno="100006", dniAlumno="58098730"},
                    new AlumnoBE() {codAlumno="100007", dniAlumno="77772638"},
                    new AlumnoBE() {codAlumno="100008", dniAlumno="65220218"},
                };

                List<ProfesorBE> Profesores = new List<ProfesorBE>()
                {
                    new ProfesorBE() {codProfesor="200000", dniProfesor="71000575"},
                    new ProfesorBE() {codProfesor="200001", dniProfesor="46110945"},
                    new ProfesorBE() {codProfesor="200002", dniProfesor="66732448"},
                    new ProfesorBE() {codProfesor="200003", dniProfesor="65867475"},
                };

                List<AdministradorBE> Administradores = new List<AdministradorBE>()
                {
                    new AdministradorBE() {codAdministrador="300000", dniAdministrador="71755575"},
                };

                List<CursoBE> Cursos = new List<CursoBE>()
                {
                    new CursoBE() { idCurso="400000", nombreCurso="Programacion 1",descripcionCurso="Este curso proporciona al alumno una sólida base de conocimientos, imprescindible para iniciarse en el mundo de la programación.",codProfesor="200000"},
                    new CursoBE() { idCurso="400001", nombreCurso="Java desde cero",descripcionCurso="Conoceras la sintaxis y el flujo de trabajo de Java. Aprenderemos los tipos de datos básicos, condicionales y ciclos del lenguaje y también aprenderemos a compilar el código fuente.",codProfesor="200001"},
                    new CursoBE() { idCurso="400002", nombreCurso="Node.js desde cero",descripcionCurso="Domina JavaScript en el lado del servidor y sé un programador full stack.",codProfesor="200002"},
                    new CursoBE() { idCurso="400003", nombreCurso="Programación orientada a objetos con JavaScript",descripcionCurso="La programación orientada a objetos es un paradigma que desde la versión ES6 puede usarse en JavaScript a través de clases.",codProfesor="200003"},
                    new CursoBE() { idCurso="400004", nombreCurso="Angular Desde Cero",descripcionCurso="Angular es un poderoso framework javascript para el desarrollo de proyectos web que nos brinda herramientas optimas y actuales como templates declarativos, Event Binding, injección de dependencias.",codProfesor="200002"},
                };

                List<AlumnoCursoBE> AlumnoCursos = new List<AlumnoCursoBE>()
                {
                    new AlumnoCursoBE() { codAlumno="100000",idCurso="400000",numVoucher="500000"},
                    new AlumnoCursoBE() { codAlumno="100000",idCurso="400001",numVoucher="500000"},
                    new AlumnoCursoBE() { codAlumno="100001",idCurso="400002",numVoucher="500000"},
                    new AlumnoCursoBE() { codAlumno="100001",idCurso="400004",numVoucher="500000"},
                    new AlumnoCursoBE() { codAlumno="100002",idCurso="400002",numVoucher="500000"},
                    new AlumnoCursoBE() { codAlumno="100003",idCurso="400003",numVoucher="500000"},
                    new AlumnoCursoBE() { codAlumno="100005",idCurso="400000",numVoucher="500000"},
                    new AlumnoCursoBE() { codAlumno="100007",idCurso="400000",numVoucher="500000"},
                    new AlumnoCursoBE() { codAlumno="100008",idCurso="400002",numVoucher="500000"},
                    new AlumnoCursoBE() { codAlumno="100003",idCurso="400004",numVoucher="500000"},
                    new AlumnoCursoBE() { codAlumno="100006",idCurso="400000",numVoucher="500000"},
                    new AlumnoCursoBE() { codAlumno="100004",idCurso="400000",numVoucher="500000"},
                };

                List<CapituloBE> Capitulos = new List<CapituloBE>()
                {
                    new CapituloBE() { numCapitulo=1,tituloCapitulo="Algoritmo y Estructura de Datos Básico",idCurso="400000"},
                    new CapituloBE() { numCapitulo=2,tituloCapitulo="Variables y tipos de datos",idCurso="400000"},
                    new CapituloBE() { numCapitulo=3,tituloCapitulo="Operadores",idCurso="400000"},

                    new CapituloBE() { numCapitulo=1,tituloCapitulo="Introducción a Java",idCurso="400001"},
                    new CapituloBE() { numCapitulo=2,tituloCapitulo="Operadores y Condicionales",idCurso="400001"},
                    new CapituloBE() { numCapitulo=3,tituloCapitulo="Ciclos",idCurso="400001"},

                    new CapituloBE() { numCapitulo=1,tituloCapitulo="Fundamentos de Node.js",idCurso="400002"},
                    new CapituloBE() { numCapitulo=2,tituloCapitulo="Crear un servidor http básico",idCurso="400002"},
                    new CapituloBE() { numCapitulo=3,tituloCapitulo="Creación de API rest con node.js",idCurso="400002"},

                    new CapituloBE() { numCapitulo=1,tituloCapitulo="Conceptos de POO",idCurso="400003"},
                    new CapituloBE() { numCapitulo=2,tituloCapitulo="Clases y objetos ",idCurso="400003"},
                    new CapituloBE() { numCapitulo=3,tituloCapitulo="Manejo y comunicación de los objetos",idCurso="400003"},

                    new CapituloBE() { numCapitulo=1,tituloCapitulo="Introducción a Angular",idCurso="400004"},
                    new CapituloBE() { numCapitulo=2,tituloCapitulo="Tu Primera Aplicación Angular",idCurso="400004"},
                    new CapituloBE() { numCapitulo=3,tituloCapitulo="Componentes y Enrutamiento",idCurso="400004"},
                };


                context.DbUsuario.AddRange(Usuarios);
                context.DbAlumno.AddRange(Alumnos);
                context.DbProfesor.AddRange(Profesores);
                context.DbAdministrador.AddRange(Administradores);
                context.DbCurso.AddRange(Cursos);
                context.DbAlumnoCurso.AddRange(AlumnoCursos);
                context.DBCapitulo.AddRange(Capitulos);

                base.Seed(context);
            }
            catch (DbEntityValidationException e)
            {
                foreach (var validationErrors in e.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
            }
        }
    }
}
