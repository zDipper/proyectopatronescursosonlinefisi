﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using CapaEntidad;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace CapaDeDatos
{
    public class ContextDA:DbContext
    {
        // definomos la cadena de conexion
        public ContextDA() : base("cursosonline")
        {
            // si la base de datos no existe se procede a crear y cargar la base de datos
            if (!this.Database.Exists())
            {
                Database.SetInitializer(new CargaInicial());

                if (!this.Database.Exists()) this.Database.Initialize(true);

                //this.Configuration.ProxyCreationEnabled = false;
            }
                
        }
        // creamos las tablas de la base de datos
        public DbSet<UsuarioBE> DbUsuario { get; set; }

        public DbSet<ProfesorBE> DbProfesor { get; set; }

        public DbSet<AlumnoBE> DbAlumno { get; set; }

        public DbSet<AdministradorBE> DbAdministrador { get; set; }

        public DbSet<CursoBE> DbCurso { get; set; }

        public DbSet<AlumnoCursoBE> DbAlumnoCurso { get; set; }

        public DbSet<CapituloBE> DBCapitulo { get; set; }


        // para algunas modificaciones que tiene por defecto
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            // con esto creamos una llave compuesta
            modelBuilder.Entity<AlumnoCursoBE>().HasKey(table => new { table.codAlumno, table.idCurso });
        }


    }


}
