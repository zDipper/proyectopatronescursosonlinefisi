﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaEntidad;

namespace CapaDeDatos
{
    public class AlumnoCursoDA
    {
        ContextDA context = new ContextDA();
        public List<AlumnoCursoBE> listarCursosAlumno(string codAlumno)
        {
            try
            {
                return context.DbAlumnoCurso.Where(c => c.codAlumno == codAlumno).ToList();
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                    }
                }
                return new List<AlumnoCursoBE>();
            }

        }
    }
}
