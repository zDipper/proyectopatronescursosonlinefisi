﻿using CapaEntidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDeDatos
{
    public class AdministradorDA
    {
        ContextDA context = new ContextDA();

        public AdministradorBE ObtenerAdministradorDni(string dni)
        {

            try
            {
                return context.DbAdministrador.Where(v => v.dniAdministrador == dni).Single();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new AdministradorBE();
            }
        }
    }
}
