﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidad
{
    //creacion de la entidad Usuario
    [Table("usuario")]
    public class UsuarioBE
    {
        #region Propiedades
        [Column("dniUsuario", TypeName = "varchar"), MaxLength(8), Key]
        public string DniUsuario { get; set; }

        [Column("nombresUsuario" ,TypeName = "varchar"), MaxLength(50),Required]
        public string NombresUsuario { get; set; }

        [Column("apellidosUsuario", TypeName = "varchar"), MaxLength(50), Required]
        public string ApellidosUsuario { get; set; }

        [Column("correoUsuario", TypeName = "varchar"), MaxLength(50), Required]
        public string CorreoUsuario { get; set; }

        [Column("contrasenaUsuario", TypeName = "varchar"), MaxLength(50), Required]
        public string ContrasenaUsuario { get; set; }

        #endregion

    }
}
