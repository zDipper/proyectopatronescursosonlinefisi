﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidad
{
    //creacion de la entidad AlumnoCurso
    [Table("alumnoCurso")]
    public class AlumnoCursoBE
    {
        [Column("codAlumno", TypeName = "varchar"), MaxLength(6), Key]
        public string codAlumno { set; get; }
        /*
        [ForeignKey("codAlumno")]
        public virtual AlumnoBE AlumnoBE { set; get; }*/

        [Column("idCurso", TypeName = "varchar"), MaxLength(6), Key]
        public string idCurso { set; get; }
        /*
        [ForeignKey("idCurso")]
        public virtual CursoBE CursoBE { set; get; }*/

        [Column("numVoucher", TypeName = "varchar"),MaxLength(6) ]
        public string numVoucher { set; get; }
    }
}
