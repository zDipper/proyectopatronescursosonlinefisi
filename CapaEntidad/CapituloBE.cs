﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidad
{
    //creacion de la entidad Capitulo
    [Table("capitulo")]
    public class CapituloBE
    {
        [Column("idCapitulo",TypeName ="int"),Key]
        public int idCapitulo { set; get; }

        [Column("numCapitulo", TypeName = "int")]
        public int numCapitulo { set; get; }

        [Column("tituloCapitulo", TypeName = "varchar"),MaxLength(50)]
        public string tituloCapitulo { set; get; }

        [Column("idCurso", TypeName = "varchar"), MaxLength(6)]
        public string idCurso { set; get; }
        /*
        [ForeignKey("idCurso")]
        public virtual CursoBE Curso { get; set; }*/
    }
}
