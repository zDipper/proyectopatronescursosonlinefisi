﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidad
{
    //creacion de la entidad Profesor
    [Table("profesor")]
    public class ProfesorBE
    {
        [Column("codProfesor", TypeName = "varchar"), MaxLength(6),Key]
        public string codProfesor { set; get; }

        [Column("dniProfesor", TypeName = "varchar"), MaxLength(8)]
        public string dniProfesor { set; get; }

       /* [ForeignKey("dniProfesor")]
        public virtual UsuarioBE UsuarioBE { get; set; }*/
    }
    
}
