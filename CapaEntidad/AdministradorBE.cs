﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidad
{
    //creacion de la entidad Administrador
    [Table("administrador")]
    public class AdministradorBE
    {
        [Column("codAdministrador", TypeName = "varchar"), MaxLength(6), Key]
        public string codAdministrador { set; get; }

        [Column("dniAdministrador", TypeName = "varchar"), MaxLength(8)]
        public string dniAdministrador { set; get; }

        /*[ForeignKey("dniAdministrador")]
        public virtual UsuarioBE UsuarioBE { get; set; }*/
    }
}
