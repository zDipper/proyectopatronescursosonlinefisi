﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidad
{
    //creacion de la entidad Curso
    [Table("curso")]
    public class CursoBE
    {
        [Column("idCurso", TypeName = "varchar"), MaxLength(6), Key]
        public string idCurso { set; get; }

        [Column("nombreCurso", TypeName = "varchar"), MaxLength(50)]
        public string nombreCurso { set; get; }

        [Column("descripcionCurso", TypeName = "varchar"), MaxLength(400)]
        public string descripcionCurso { set; get; }

        [Column("codProfesor", TypeName = "varchar"), MaxLength(6)]
        public string codProfesor { set; get; }
        /*
        [ForeignKey("codProfesor")]
        public virtual ProfesorBE Profesor { get; set; }*/
    }
}
