﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CapaEntidad
{
    //creacion de la entidad Alumno
    [Table("alumno")]
    public class AlumnoBE
    {
        [Column("codAlumno", TypeName = "varchar"), MaxLength(6), Key]
        public string codAlumno { set; get; }

        [Column("dniAlumno",TypeName ="varchar"),MaxLength(8)]
        public string dniAlumno { set;get; }
        /*
        [ForeignKey("dniAlumno")]
        public virtual UsuarioBE UsuarioBE { get; set; }*/
    }
}
