﻿using CapaEntidad;
using CapaLogica;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace CapaWebPresentacion
{
    /// <summary>
    /// Descripción breve de wsCursos
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    [System.Web.Script.Services.ScriptService]
    public class wsCursos : System.Web.Services.WebService
    {
        [WebMethod]
        public List<CursoBE> ObtenerCursos()
        {
            CursoBL cursoBL = new CursoBL();
            return cursoBL.ListarCursos();
        }

        [WebMethod]
        public List<CursoBE> ObtenerCursosXNombre(string nombre)
        {
            CursoBL cursoBL = new CursoBL();
            return cursoBL.obtenerCursosXNombre(nombre);
        }

        [WebMethod]
        public List<CursoBE> ObtenerCursosProfesor(string codProfe)
        {
            CursoBL cursoBL = new CursoBL();
            return cursoBL.ObtenerCursosProfe(codProfe);
        }
    }

}
