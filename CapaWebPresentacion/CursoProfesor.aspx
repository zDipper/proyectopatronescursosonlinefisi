﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CursoProfesor.aspx.cs" Inherits="CapaWebPresentacion.CursoProfesor" %>

    <!DOCTYPE html>

    <html xmlns="http://www.w3.org/1999/xhtml">

    <head runat="server">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Cursos Online Cerseu</title>
        <link href="Content/bootstrap.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/solid.css" />
        <script src="https://use.fontawesome.com/releases/v5.0.7/js/all.js"></script>
        <script src="Scripts/umd/popper.min.js"></script>
        <script src="Scripts/jquery-3.4.1.min.js"></script>
        <script src="Scripts/bootstrap.min.js"></script>
        <link href="static/css/main.css" rel="stylesheet" />
    </head>

    <body>
        <header class="encabezado-Inicio">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand" href="Profesor.aspx"><img class="logo" src="static/img/cerseu.png" alt="logo Cerseu"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto mr-5">
                        <li class="nav-item">
                            <a class="nav-link" href="MisCursosProfesor.aspx">Mis Cursos</a>
                        </li>
                    </ul>
                    <div class="btn-group">
                        <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" data-display="static" aria-haspopup="true" aria-expanded="false">Configuracion</button>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg-left">
                            <button id="perfilProfesor" class="dropdown-item" type="button">Perfil</button>
                            <button class="dropdown-item" id="cerrarSesion" type="button">Cerrar sesion</button>
                        </div>
                    </div>
                </div>
            </nav>
        </header>

        <section class="vistaCurso container">
            <div id="TituloCursoProfesor">
            </div>
            <div id="contenidoCursoProfesor">
            </div>
        </section>

        <footer class="site-footer ">
            <div class="container clearfix ">
                <div class="footer-informacion ">
                    <h2>Sobre nosotros</h2>
                    <p>El Centro de Responsabilidad Social y Extensión Universitaria es el órgano de línea encargado de integrar a la Facultad de Ingeniería de Sistemas e Informática a la comunidad, a través de la organización de actividades de cultura general,
                        carácter profesional no escolarizado y prestación de servicios, con el apoyo de docentes y estudiantes.</p>
                    <p>La Extensión como vehículo ideal en la interacción recíproca Universidad-Sociedad, se erige como una labor social para la construcción de pertinencia social de la Institución en el sentido de cumplir con el deber educativo de formar
                        y desarrollar las potencialidades del estudiante, ofreciéndole para ello sensibilidad, competencias en los avances científicos y tecnológicos y escenarios propicios para su evolución y servicio a la comunidad.</p>
                </div>
                <div class="direccion ">
                    <div class="direccion-abajo ">
                        <p><i class="fas fa-map-marker-alt icono "></i>Ciudad Universitaria - Av. Germán Amézaga</p>
                        <p><i class="fas fa-phone icono "></i>Teléfono: 619 - 7000 Anexo 3615</p>
                        <p><i class="fas fa-at icono "></i>informatica.fisi@unmsm.edu.pe</p>
                    </div>
                </div>
            </div>
        </footer>

        <script src="static/js/main.js "></script>
        <script src="static/js/profesor.js "></script>
        <script src="static/js/capituloProfesor.js"></script>
    </body>

    </html>