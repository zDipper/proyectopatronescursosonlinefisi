﻿using CapaLogica;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using CapaEntidad;

namespace CapaWebPresentacion
{
    /// <summary>
    /// Descripción breve de wsProfesor
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    [System.Web.Script.Services.ScriptService]
    public class wsProfesor : System.Web.Services.WebService
    {

        [WebMethod]
        public ProfesorBE ObtenerProfesorDni(string dni)
        {
            ProfesorBL ProfesorBL = new ProfesorBL();
            return ProfesorBL.ObtenerProfesorDni(dni);
        }
    }
}
