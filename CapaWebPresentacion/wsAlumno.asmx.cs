﻿using CapaEntidad;
using CapaLogica;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace CapaWebPresentacion
{
    /// <summary>
    /// Descripción breve de wsAlumno
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    [System.Web.Script.Services.ScriptService]
    public class wsAlumno : System.Web.Services.WebService
    {

        [WebMethod]
        public AlumnoBE ObtenerAlumnoDni(string dni)
        {
            AlumnoBL alumnoBL = new AlumnoBL();
            return alumnoBL.ObtenerAlumnoDni(dni);
        }

        [WebMethod]
        public List<CursoBE> listarCursosAlumno(string codAlumno)
        {
            AlumnoCursoBL alumnoCursoBL = new AlumnoCursoBL();
            CursoBL cursoBL = new CursoBL();
            List<AlumnoCursoBE> alumnoCursoBEs = alumnoCursoBL.listarCursosAlumno(codAlumno);
            List<CursoBE> cursoBEs = new List<CursoBE>();
            foreach (AlumnoCursoBE item in alumnoCursoBEs)
            {
                cursoBEs.Add(cursoBL.ObtenerCurso(item.idCurso));
            }
            return cursoBEs;
        }


        [WebMethod]
        public bool actualizarUsuario(UsuarioBE objUsuario)
        {
            UsuarioBL objUsuarioBL = new UsuarioBL();

            return objUsuarioBL.ActualizarUsuario(objUsuario);
        }
        [WebMethod]
        public string ultimoAlumno()
        {
            AlumnoBL alumnoBL = new AlumnoBL();

            return alumnoBL.ObtenerUCodigoAlumno();
        }

        [WebMethod]
        public bool agregarAlumno(AlumnoBE objAlumno)
        {
            AlumnoBL alumnoBL = new AlumnoBL();
            String cod = objAlumno.codAlumno;
            String mensaje = "";
            return alumnoBL.GuardarAlumno(objAlumno, out cod, out mensaje);
        }



    }
}
