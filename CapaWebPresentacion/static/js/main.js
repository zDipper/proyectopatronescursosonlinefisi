﻿$(function() {
    obtenerCursosInicio();
    obtenerUsuario();
    irBusquedaInicio();
    irBusqueda();
    Registrarse();
    soloNumeros();
});

// funcion para obtener y mostrar los cursos disponibles
function obtenerCursosInicio() {
    $.ajax({
        method: "POST",
        url: "http://localhost:50088/wsCursos.asmx/ObtenerCursos",
        type: "json",
        contentType: "application/json; charset=utf-8",
        success: function(resp) {
            //console.log(resp);
            $("#contenedorInicio").empty();
            $.each(resp.d, function(p, d) {
                //console.log(d);
                $("#contenedorInicio").append("<div class='col-sm-4'>" +
                    "<div class='card'>" +
                    "<img src = 'static/img/java desde cero.jpg' class = 'card-img-top' alt = " + d.nombreCurso + " />" +
                    "<div class = 'card-body' > " +
                    "<h5 class = 'card-title' > " + d.nombreCurso + "</h5> " +
                    "<p class = 'card-text' >" + d.descripcionCurso + "</p> " +
                    "<div class='text-right'>" +
                    "<a href = '#' class = 'btn btn-info'id='todo-" + d.idCurso + "'>ver mas</a>" +
                    "</div> " +
                    "</div> " +
                    "</div> " +
                    "</div >");
                $("#todo-" + d.idCurso).click(function(e) {
                    //console.log("ashdg");
                    var Curso = {
                        "codCurso": d.idCurso,
                        "nombreCurso": d.nombreCurso,
                        "descripcionCurso": d.descripcionCurso,
                    };
                    localStorage.setItem("Curso", JSON.stringify(Curso));
                    window.location.href = 'VistaCursoInicio.aspx';
                });
            });

        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log(errorThrown);
        }
    });
}


// funcion para obtener un usuario
function obtenerUsuario() {
    $("#btnIniciar").click(function(e) {
        e.preventDefault();
        //console.log("asjhdvg");
        var data1 = $("#inputEmailInicio").val();
        var data2 = $("#inputPasswordInicio").val();
        //console.log(data1);
        //console.log(data2);

        var dni;
        var regex = /^([\da-z_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/;
        if (!regex.test(data1)) {
            $("#alertaEmail").empty();
            $("#alertaEmail").append(
                '<div class="alert alert-light alert-dismissible fade show" role="alert">' +
                '<strong>Email Incorrecto !</strong> Intente  otra vez' +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
                '</div>');
            $("#inputEmailInicio").val("");
            $("#inputPasswordInicio").val("");
        } else {
            $.ajax({
                method: "POST",
                url: "http://localhost:50088/wsUsuario.asmx/ObtenerUsuarioCuenta",
                // data: "{ correo: '" + data1 + "', password: " + data2 + "}",
                data: JSON.stringify({ 'correo': data1, 'password': data2 }),
                DataType: "json",
                type: "json",
                contentType: "application/json; charset=utf-8",
                success: function(resp) {
                    //console.log("geeee");
                    $.each(resp, function(i, item) {
                        dni = item.DniUsuario;
                        var Usuario = {
                            "dniUsuario": item.DniUsuario,
                            "nombresUsuario": item.NombresUsuario,
                            "apellidosUsuario": item.ApellidosUsuario,
                            "correoUsuario": item.CorreoUsuario,
                            "contrasenaUsuario": item.ContrasenaUsuario
                        };
                        localStorage.setItem("Usuario", JSON.stringify(Usuario));
                        if (dni == null) {
                            //console.log(dni);
                            $("#alerta").empty();
                            $("#alerta").append('<div class= "alert alert-warning" role = "alert" >' +
                                "correo o contraseña incorrecta" +
                                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                                '<span aria-hidden="true">&times;</span>' +
                                '</button>' +
                                '</div>');
                            $("#inputEmailInicio").val("");
                            $("#inputPasswordInicio").val("");
                        } else {
                            //console.log(dni);
                            obtenerAlumno(dni);
                            //obtenerProfesor(dni);
                            //obtenerAdministrador(dni);
                        }
                    });
                },
                error: function(r) {
                    alert(r.responseText);
                },
                failure: function(r) {
                    alert(r.responseText);
                }
            });
        }
    });
}

function soloNumeros() {
    $(".soloNumero").keydown(function(event) {
        //alert(event.keyCode);
        if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105) && event.keyCode !== 190 && event.keyCode !== 110 && event.keyCode !== 8 && event.keyCode !== 9) {
            return false;
        }
    });
}
/*
function emailDiferente(email) {

    $.ajax({
        method: "POST",
        url: "http://localhost:50088/wsUsuario.asmx/obtenerEmail",
        data: JSON.stringify({ 'correo': email }),
        DataType: "json",
        type: "json",
        contentType: "application/json; charset=utf-8",
        success: function(resp) {
            if (resp.d) {
                return true;
            } else {
                return false;
            }
        },
        error: function(r) {
            console.log(r.responseText);
        },
        failure: function(r) {
            console.log(r.responseText);
        }
    });
}*/


function Registrarse() {
    $("#registrar").click(function(e) {
        e.preventDefault();

        var nombres = $("#registroNombre").val();
        var apellidos = $("#registroApellidos").val();
        var dni = $("#registroDni").val();
        var correo = $("#registroEmail").val();
        var password = $("#RegistroPassword").val();
        if (noNumeros(nombres) && nombres != "") {
            if (noNumeros(apellidos) && apellidos != "") {
                if (dni.length == 8 && dni != "") {
                    var regex = /^([\da-z_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/;
                    if (regex.test(correo) && correo != "") {
                        if (password != "") {
                            var usuario = {
                                "dniUsuario": dni,
                                "nombresUsuario": nombres,
                                "apellidosUsuario": apellidos,
                                "correoUsuario": correo,
                                "contrasenaUsuario": password
                            }
                            console.log(usuario);

                            $.ajax({
                                method: "POST",
                                url: "http://localhost:50088/wsUsuario.asmx/agregarUsuario",
                                data: JSON.stringify({ 'objUsuario': usuario }),
                                type: "json",
                                contentType: "application/json; charset=utf-8",
                                success: function(resp) {
                                    alert("Alumno Registrado");
                                    localStorage.setItem("Usuario", JSON.stringify(usuario));
                                    console.log("xcshjd");

                                    registrarAlumno(dni);
                                },
                                error: function(r) {
                                    console.log(r.responseText);
                                },
                                failure: function(r) {
                                    console.log(r.responseText);
                                }
                            });
                        } else {
                            $("#alertaPasword").empty();
                            $("#alertaPasword").append(
                                '<div class="alert alert-light alert-dismissible fade show" role="alert">' +
                                'no puede ser un campo vacio' +
                                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                                '<span aria-hidden="true">&times;</span>' +
                                '</button>' +
                                '</div>');
                            $("#RegistroPassword").val("");
                        }
                    } else {
                        $("#alertaCorreo").empty();
                        $("#alertaCorreo").append(
                            '<div class="alert alert-light alert-dismissible fade show" role="alert">' +
                            '<strong>Email Incorrecto !</strong> Intente  otra vez' +
                            '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                            '<span aria-hidden="true">&times;</span>' +
                            '</button>' +
                            '</div>');
                        $("#registroEmail").val("");
                    }
                } else {
                    $("#alertaDNI").empty();
                    $("#alertaDNI").append(
                        '<div class="alert alert-light alert-dismissible fade show" role="alert">' +
                        'Deben ser 8 digitos y no debe contener campo vacio' +
                        '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                        '<span aria-hidden="true">&times;</span>' +
                        '</button>' +
                        '</div>');
                    $("#registroDni").val("");
                }
            } else {
                $("#alertaApellidos").empty();
                $("#alertaApellidos").append(
                    '<div class="alert alert-light alert-dismissible fade show" role="alert">' +
                    'No se permite Numeros ni campo vacio' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>');
                $("#registroApellidos").val("");
            }
        } else {
            $("#alertaNombre").empty();
            $("#alertaNombre").append(
                '<div class="alert alert-light alert-dismissible fade show" role="alert">' +
                'No se permite Numeros ni campo vacio' +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
                '</div>');
            $("#registroNombre").val("");
        }

    });
}

function noNumeros(cadena) {
    if (/^[A-Z]+$/i.test(cadena)) {
        return true;
    } else {
        return false;
    }
}

function noLetras(cadena) {
    if (/^[A-Z]+$/i.test(cadena)) {
        return true;
    } else {
        return false;
    }
}

function ultimoAlumno() {
    $.ajax({
        method: "POST",
        url: "http://localhost:50088/wsAlumno.asmx/ultimoAlumno",
        type: "json",
        contentType: "application/json; charset=utf-8",
        success: function(resp) {
            var UltimoAlumno = {
                "numAlumnos": resp.d,
            };
            localStorage.setItem("UltimoAlumno", JSON.stringify(UltimoAlumno));
        },
        error: function(r) {
            console.log(r.responseText);
        },
        failure: function(r) {
            console.log(r.responseText);
        }
    });
    var UltimoAlumno = JSON.parse(localStorage.getItem("UltimoAlumno"));
    return parseInt(UltimoAlumno.numAlumnos) + 1;
}

function registrarAlumno(dni) {
    var Alumno = {
        "codAlumno": ultimoAlumno().toString(),
        "dniAlumno": dni,
    };
    $.ajax({
        method: "POST",
        url: "http://localhost:50088/wsAlumno.asmx/agregarAlumno",
        data: JSON.stringify({ 'objAlumno': Alumno }),
        type: "json",
        contentType: "application/json; charset=utf-8",
        success: function(resp) {
            localStorage.setItem("Alumno", JSON.stringify(Alumno));
            window.location.href = 'Alumno.aspx';
        },
        error: function(r) {
            console.log(r.responseText);
        },
        failure: function(r) {
            console.log(r.responseText);
        }
    });
}

function obtenerAlumno(dniAl) {
    var dniA = dniAl;
    var al;
    $.ajax({
        method: "POST",
        url: "http://localhost:50088/wsAlumno.asmx/ObtenerAlumnoDni",
        data: "{ dni: " + dniA + "}",
        DataType: "json",
        type: "json",
        contentType: "application/json; charset=utf-8",
        success: function(resp) {
            $.each(resp, function(i, item) {
                //console.log(item);
                al = item.codAlumno;
                if (al !== null) {
                    var Alumno = {
                        "codAlumno": item.codAlumno,
                        "dniAlumno": item.dniAlumno,
                    };
                    localStorage.setItem("Alumno", JSON.stringify(Alumno));
                    window.location.href = 'Alumno.aspx';
                } else {
                    obtenerProfesor(dniA);
                }
            });
        },
        error: function(r) {
            console.log(r.responseText);
        },
        failure: function(r) {
            console.log(r.responseText);
        }
    });
}

function irBusquedaInicio() {
    $("#buscarInicio").click(function(e) {
        var nom = $("#inputBuscar").val();
        var busca = {
            "nom": nom,
        };
        localStorage.setItem("busca", JSON.stringify(busca));
        window.location.href = 'BusquedaInicio.aspx';
    })
}

function irBusqueda() {
    $("#buscarAlumno").click(function(e) {
        var nom = $("#inputBuscar").val();
        var busca = {
            "nom": nom,
        };
        localStorage.setItem("buscaAlumno", JSON.stringify(busca));
        window.location.href = 'BusquedaAlumno.aspx';
    })
}

function obtenerProfesor(dniProfe) {
    var dniP = dniProfe;
    var pro;
    $.ajax({
        method: "POST",
        url: "http://localhost:50088/wsProfesor.asmx/ObtenerProfesorDni",
        data: "{ dni: " + dniP + "}",
        DataType: "json",
        type: "json",
        contentType: "application/json; charset=utf-8",
        success: function(resp) {
            $.each(resp, function(i, item) {
                console.log(item);
                pro = item.codProfesor;
                if (pro !== null) {
                    var Profesor = {
                        "codProfesor": item.codProfesor,
                        "dniProfesor": item.dniProfesor,
                    };
                    localStorage.setItem("Profesor", JSON.stringify(Profesor));
                    window.location.href = 'Profesor.aspx';
                } else {
                    obtenerAdministrador(dniP);
                }
            });
        },
        error: function(r) {
            console.log(r.responseText);
        },
        failure: function(r) {
            console.log(r.responseText);
        }
    })
}