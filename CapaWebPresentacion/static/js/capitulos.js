﻿$(function() {
    console.log("gagdhag");
    cargarCurso();
    cargarCursoInicio();
});

// cargar usuario alumno
function cargarCursoInicio() {
    var curso = JSON.parse(localStorage.getItem("Curso"));
    console.log(curso);
    $("#TituloCursoInicio").empty();
    $("#TituloCursoInicio").append("<h1>" + curso.nombreCurso + "</h1>");
    obtenerCapitulos(curso.codCurso);
}

function cargarCurso() {
    var curso = JSON.parse(localStorage.getItem("Curso"));
    console.log(curso);
    $("#TituloCurso").empty();
    $("#TituloCurso").append("<h1>" + curso.nombreCurso + "</h1>");
    $("#matricula").empty();
    $("#matricula").append(
        '<a href="#" class="btn btn-info" data-toggle="modal" data-target="#Matricula">Matricularme</a>'
    );
    $("#codCurso").val("");
    $("#codCurso").val(curso.codCurso);
    obtenerCapitulos(curso.codCurso);
}


function obtenerCapitulos(codCurso) {
    console.log("aea");
    $.ajax({
        method: "POST",
        url: "http://localhost:50088/wsCapitulos.asmx/obtenerCapituloXCurso",
        data: JSON.stringify({ 'codCurso': codCurso }),
        type: "json",
        contentType: "application/json; charset=utf-8",
        success: function(resp) {
            console.log(resp);
            $("#contenidoCurso").empty();
            $.each(resp.d, function(p, d) {
                $("#contenidoCurso").append(
                    "<div class='card mb-4'>" +
                    "<div class='row no-gutters'>" +
                    "<div class='col-auto p-3 '>" +
                    "<h4 class='card-title'>Tema " + d.numCapitulo + " :</h4>" +
                    "</div>" +
                    "<div class='col'>" +
                    "<div class='card-block p-2'>" +
                    "<h4 class='card-title'>" + d.tituloCapitulo + "</h4>" +
                    "</div>" +
                    "</div>" +
                    "</div>" +
                    "</div>");
            });
        },
        error: function(r) {
            console.log(r.responseText);
        },
        failure: function(r) {
            console.log(r.responseText);
        }
    })
}