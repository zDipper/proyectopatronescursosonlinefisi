﻿$(function() {
    console.log("ProfesorJS");
    cargarUsuarioProfesor();
    misCursosProfesor();
    cerrarSesion();
});

function cargarUsuarioProfesor() {
    var usuario = JSON.parse(localStorage.getItem("Usuario"));
    //console.log(usuario);
    var Profesor = JSON.parse(localStorage.getItem("Profesor"));
    //console.log(Profesor);
    $("#bienvenida").empty();
    $("#bienvenida").append("<h1> Bienvenido(a) " + usuario.nombresUsuario + " !! </h1>");
    obtenerCursosProfesor(Profesor.codProfesor);
}

function obtenerCursosProfesor(codProfesor) {
    console.log("obtenerCursosProfe");

    $.ajax({
        method: "POST",
        url: "http://localhost:50088/wsCursos.asmx/ObtenerCursosProfesor",
        data: JSON.stringify({ 'codProfe': codProfesor }),
        type: "json",
        contentType: "application/json; charset=utf-8",
        success: function(resp) {
            //console.log(resp);
            $("#contenedorMisCursosprofesor").empty();
            $.each(resp.d, function(p, d) {
                //console.log(d.nombreCurso);
                $("#contenedorMisCursosprofesor").append("<div class='col-sm-4'>" +
                    "<div class='card'>" +
                    "<img src = 'static/img/java desde cero.jpg' class = 'card-img-top' alt = " + d.nombreCurso + " />" +
                    "<div class = 'card-body' > " +
                    "<h5 class = 'card-title' > " + d.nombreCurso + "</h5> " +
                    "<p class = 'card-text' >" + d.descripcionCurso + "</p> " +
                    "<div class='text-right'>" +
                    "<a href = '#' class = 'btn btn-info' id='profesor-" + d.idCurso + "' > Ir al Curso </a>" +
                    "</div> " +
                    "</div> " +
                    "</div> " +
                    "</div >"
                );
                $("#profesor-" + d.idCurso).click(function(e) {
                    var Curso = {
                        "codCurso": d.idCurso,
                        "nombreCurso": d.nombreCurso,
                        "descripcionCurso": d.descripcionCurso,
                    };

                    localStorage.setItem("Curso", JSON.stringify(Curso));
                    window.location.href = 'CursoProfesor.aspx';
                });
            });
        },
        error: function(r) {
            console.log(r.responseText);
        },
        failure: function(r) {
            console.log(r.responseText);
        }
    })
}

function misCursosProfesor() {

    var Profesor = JSON.parse(localStorage.getItem("Profesor"));
    console.log("misCursosProfesor");

    $.ajax({
        method: "POST",
        url: "http://localhost:50088/wsCursos.asmx/ObtenerCursosProfesor",
        data: JSON.stringify({ 'codProfe': Profesor.codProfesor }),
        type: "json",
        contentType: "application/json; charset=utf-8",
        success: function(resp) {
            //console.log(resp);
            $("#misCursosProfesor").empty();
            $.each(resp.d, function(p, d) {
                //console.log(d.nombreCurso);
                $("#misCursosProfesor").append('<div class="card">' +
                    '<div class="row no-gutters">' +
                    '<div class="col-auto card-img">' +
                    '<img src="static/img/java desde cero.jpg" class="img-fluid" alt="java desde cero">' +
                    '</div>' +
                    '<div class="col">' +
                    '<div class="card-block p-2">' +
                    '<h4 class="card-title p-2">' + d.nombreCurso + '</h4>' +
                    '<p class="card-text p-4">' + d.descripcionCurso + '</p>' +
                    '<div class="text-right">' +
                    '<a href="#" class="btn btn-info" id="Profesor-' + d.idCurso + '">ir curso</a>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>'
                );
                $("#Profesor-" + d.idCurso).click(function(e) {
                    var Curso = {
                        "codCurso": d.idCurso,
                        "nombreCurso": d.nombreCurso,
                        "descripcionCurso": d.descripcionCurso,
                    };
                    localStorage.setItem("Curso", JSON.stringify(Curso));
                    window.location.href = 'CursoProfesor.aspx';
                });
            });
        },
        error: function(r) {
            console.log(r.responseText);
        },
        failure: function(r) {
            console.log(r.responseText);
        }
    })
}

function cerrarSesion() {
    $("#cerrarSesion").click(function(e) {
        localStorage.clear();
        window.location.href = 'Inicio.aspx';
    })
}