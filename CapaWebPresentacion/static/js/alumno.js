﻿$(function() {
    console.log("gagdhag");
    cargarUsuarioAlumno();
    obtenerCursosTodo();
    llenarPerfilAlumno();
    guardarCambios();
    irPerfil();
    cerrarSesion();
    misCursosAlumno();
});
// cargar usuario alumno
function cargarUsuarioAlumno() {
    var usuario = JSON.parse(localStorage.getItem("Usuario"));
    //console.log(usuario);
    var alumno = JSON.parse(localStorage.getItem("Alumno"));
    //console.log(alumno);
    $("#bienvenida").empty();
    $("#bienvenida").append("<h1> Bienvenido(a) " + usuario.nombresUsuario + " !! </h1>");
    obtenerCursosAlumno(alumno.codAlumno);
}


// funcion para obtener y mostrar los cursos disponibles
function obtenerCursosTodo() {
    $.ajax({
        method: "POST",
        url: "http://localhost:50088/wsCursos.asmx/ObtenerCursos",
        type: "json",
        contentType: "application/json; charset=utf-8",
        success: function(resp) {
            //console.log(resp);
            $("#contenedorTodoCurso").empty();
            $.each(resp.d, function(p, d) {
                //console.log(d);
                $("#contenedorTodoCurso").append("<div class='col-sm-4'>" +
                    "<div class='card'>" +
                    "<img src = 'static/img/java desde cero.jpg' class = 'card-img-top' alt = " + d.nombreCurso + " />" +
                    "<div class = 'card-body' > " +
                    "<h5 class = 'card-title' > " + d.nombreCurso + "</h5> " +
                    "<p class = 'card-text' >" + d.descripcionCurso + "</p> " +
                    "<div class='text-right'>" +
                    "<a href = '#' class = 'btn btn-info' id='todo-" + d.idCurso + "' > ver mas </a>" +
                    "</div> " +
                    "</div> " +
                    "</div> " +
                    "</div >");
                $("#todo-" + d.idCurso).click(function(e) {
                    var Curso = {
                        "codCurso": d.idCurso,
                        "nombreCurso": d.nombreCurso,
                        "descripcionCurso": d.descripcionCurso,
                    };

                    localStorage.setItem("Curso", JSON.stringify(Curso));
                    window.location.href = 'vistaCursoAlumno.aspx';
                });
            });
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log(errorThrown);
        }
    });
}

function irPerfil() {
    $("#perfilAlumno").click(function(e) {
        window.location.href = 'PerfilAlumno.aspx';
    });
}

function obtenerCursosAlumno(codAlumno) {
    //console.log(codAlumno);
    $.ajax({
        method: "POST",
        url: "http://localhost:50088/wsAlumno.asmx/listarCursosAlumno",
        data: JSON.stringify({ 'codAlumno': codAlumno }),
        type: "json",
        contentType: "application/json; charset=utf-8",
        success: function(resp) {
            //console.log(resp);
            $("#contenedorCursosAlumno").empty();
            $.each(resp.d, function(p, d) {
                //console.log(d.nombreCurso);
                $("#contenedorCursosAlumno").append("<div class='col-sm-4'>" +
                    "<div class='card'>" +
                    "<img src = 'static/img/java desde cero.jpg' class = 'card-img-top' alt = " + d.nombreCurso + " />" +
                    "<div class = 'card-body' > " +
                    "<h5 class = 'card-title' > " + d.nombreCurso + "</h5> " +
                    "<p class = 'card-text' >" + d.descripcionCurso + "</p> " +
                    "<div class='text-right'>" +
                    "<a href = '#' class = 'btn btn-info' id='alumno-" + d.idCurso + "' > Ir al Curso </a>" +
                    "</div> " +
                    "</div> " +
                    "</div> " +
                    "</div >"
                );
                $("#alumno-" + d.idCurso).click(function(e) {
                    var Curso = {
                        "codCurso": d.idCurso,
                        "nombreCurso": d.nombreCurso,
                        "descripcionCurso": d.descripcionCurso,
                    };

                    localStorage.setItem("Curso", JSON.stringify(Curso));
                    window.location.href = 'CursoAlumno.aspx';
                });
            });
        },
        error: function(r) {
            console.log(r.responseText);
        },
        failure: function(r) {
            console.log(r.responseText);
        }
    })
}

function llenarPerfilAlumno() {
    //console.log("llenarPerfil");
    var usuario = JSON.parse(localStorage.getItem("Usuario"));
    //console.log(usuario);
    $("#nombrePerfilAlumno").val(usuario.nombresUsuario);
    $("#apellidoPerfilAlumno").val(usuario.apellidosUsuario);
    $("#emailPerfilAlumno").val(usuario.correoUsuario);
    $("#inputPasswordInicio").val(usuario.contrasenaUsuario);
}

function guardarCambios() {
    $("#CambiosPerfil").click(function(e) {
        var usuario = JSON.parse(localStorage.getItem("Usuario"));
        //console.log(usuario);
        usuario.nombresUsuario = $("#nombrePerfilAlumno").val();
        usuario.apellidosUsuario = $("#apellidoPerfilAlumno").val();
        usuario.correoUsuario = $("#emailPerfilAlumno").val();
        usuario.contrasenaUsuario = $("#inputPasswordInicio").val();
        $.ajax({
            method: "POST",
            url: "http://localhost:50088/wsAlumno.asmx/actualizarUsuario",
            data: JSON.stringify({ 'objUsuario': usuario }),
            type: "json",
            contentType: "application/json; charset=utf-8",
            success: function(resp) {
                alert("Cambios realizados");
                localStorage.setItem("Usuario", JSON.stringify(usuario));
            },
            error: function(r) {
                console.log(r.responseText);
            },
            failure: function(r) {
                console.log(r.responseText);
            }
        })
    });
}

function misCursosAlumno() {

    var alumno = JSON.parse(localStorage.getItem("Alumno"));
    console.log("misCursosAlumno");

    $.ajax({
        method: "POST",
        url: "http://localhost:50088/wsAlumno.asmx/listarCursosAlumno",
        data: JSON.stringify({ 'codAlumno': alumno.codAlumno }),
        type: "json",
        contentType: "application/json; charset=utf-8",
        success: function(resp) {
            //console.log(resp);
            $("#misCursos").empty();
            $.each(resp.d, function(p, d) {
                //console.log(d.nombreCurso);
                $("#misCursos").append('<div class="card">' +
                    '<div class="row no-gutters">' +
                    '<div class="col-auto card-img">' +
                    '<img src="static/img/java desde cero.jpg" class="img-fluid" alt="java desde cero">' +
                    '</div>' +
                    '<div class="col">' +
                    '<div class="card-block p-2">' +
                    '<h4 class="card-title p-2">' + d.nombreCurso + '</h4>' +
                    '<p class="card-text p-4">' + d.descripcionCurso + '</p>' +
                    '<div class="text-right">' +
                    '<a href="#" class="btn btn-info" id="alumno-' + d.idCurso + '">ir curso</a>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>'
                );
                $("#alumno-" + d.idCurso).click(function(e) {
                    var Curso = {
                        "codCurso": d.idCurso,
                        "nombreCurso": d.nombreCurso,
                        "descripcionCurso": d.descripcionCurso,
                    };

                    localStorage.setItem("Curso", JSON.stringify(Curso));
                    window.location.href = 'CursoAlumno.aspx';
                });
            });
        },
        error: function(r) {
            console.log(r.responseText);
        },
        failure: function(r) {
            console.log(r.responseText);
        }
    })
}

function cerrarSesion() {
    $("#cerrarSesion").click(function(e) {
        localStorage.clear();
        window.location.href = 'Inicio.aspx';
    })
}