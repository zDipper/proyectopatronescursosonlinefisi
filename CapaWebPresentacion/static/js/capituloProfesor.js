﻿$(function() {
    obtenerCapitulosProfesor();
});

function obtenerCapitulosProfesor() {

    var curso = JSON.parse(localStorage.getItem("Curso"));
    console.log("obtenerCapitulosProfesor");
    $("#TituloCursoProfesor").empty();
    $("#TituloCursoProfesor").append("<h1>" + curso.nombreCurso + "</h1>" +
        '<div class="text-right">' +
        '<a href="#" class="btn btn-info" id="temaNuevo">Agregar Tema</a>' +
        "</div>");
    $.ajax({
        method: "POST",
        url: "http://localhost:50088/wsCapitulos.asmx/obtenerCapituloXCurso",
        data: JSON.stringify({ 'codCurso': curso.codCurso }),
        type: "json",
        contentType: "application/json; charset=utf-8",
        success: function(resp) {
            console.log(resp);
            $("#contenidoCursoProfesor").empty();
            $.each(resp.d, function(p, d) {
                $("#contenidoCursoProfesor").append(
                    "<div class='card mb-4'>" +
                    "<div class='row no-gutters'>" +
                    "<div class='col-auto p-3 '>" +
                    "<h4 class='card-title'>Tema " + d.numCapitulo + " :</h4>" +
                    "</div>" +
                    "<div class='col'>" +
                    "<div class='card-block p-2'>" +
                    "<h4 class='card-title'>" + d.tituloCapitulo + "</h4>" +
                    '<div class="text-right">' +
                    '<a href="#" class="btn btn-info" id="tema-' + d.numCapitulo + curso.codCurso + '">Editar Tema</a>' +
                    "</div>" +
                    "</div>" +
                    "</div>" +
                    "</div>" +
                    "</div>");
                $("#tema-" + d.numCapitulo + curso.codCurso).click(function(e) {
                    console.log("ashdg");
                    var Tema = {
                        "codCurso": curso.codCurso,
                        "numCapitulo": d.numCapitulo,
                        "tituloCapitulo": d.tituloCapitulo,
                    };
                    localStorage.setItem("Tema", JSON.stringify(Tema));
                    window.location.href = 'TemaProfesor.aspx';
                });
            });
        },
        error: function(r) {
            console.log(r.responseText);
        },
        failure: function(r) {
            console.log(r.responseText);
        }
    })
}