﻿$(function() {
    cargaIncioTema();
    volverCurso();
});

function cargaIncioTema() {
    var tema = JSON.parse(localStorage.getItem("Tema"));
    console.log(tema);
    $("#tituloTema").empty();
    $("#tituloTema").append("<h1>" + tema.tituloCapitulo + "</h1>" +
        '<div class="text-right">' +
        '<button class="btn btn-info" id="volverCurso" >volver al curso</button>' +
        "</div>"
    );
    $("#contenedorTema").empty();
    $("#contenedorTema").append(
        '<video src="static/video/3. Creando un proyecto Web con Spring Boot.mp4" controls></video>' +
        '<div class="text-right">' +
        '<button class="btn btn-info" id="actualizarVideo" >Actualizar Video</button>' +
        "</div>"
    );

}

function volverCurso() {
    $("#volverCurso").click(function(e) {
        window.location.href = 'CursoProfesor.aspx';
    })

}