﻿$(function() {
    buscarXNombre();
});

function buscarXNombre() {
    var busca = JSON.parse(localStorage.getItem("busca"));
    console.log(busca.nom);
    $.ajax({
        method: "POST",
        url: "http://localhost:50088/wsCursos.asmx/ObtenerCursosXNombre",
        data: JSON.stringify({ 'nombre': busca.nom }),
        DataType: "json",
        type: "json",
        contentType: "application/json; charset=utf-8",
        success: function(resp) {
            $("#resultadosBusqueda").empty();
            if (resp.d.length == 0) {
                $("#resultadosBusqueda").append(
                    "<h5>No hay resultados para la busqueda</h5>"
                );
            } else {
                $.each(resp.d, function(p, d) {
                    $("#resultadosBusqueda").append(
                        '<div class="card">' +
                        '<div class="row no-gutters">' +
                        '<div class="col-auto card-img">' +
                        '<img src="static/img/java desde cero.jpg" class="img-fluid" alt="java desde cero">' +
                        '</div>' +
                        '<div class="col">' +
                        '<div class="card-block p-2">' +
                        '<h4 class="card-title">' + d.nombreCurso + '</h4>' +
                        '<p class="card-text">' + d.descripcionCurso + '</p>' +
                        '<div class="text-right">' +
                        '<a href="#" class="btn btn-info" id="todo-' + d.idCurso + '">ver mas</a>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>'
                    );
                    $("#todo-" + d.idCurso).click(function(e) {
                        console.log("ashdg");
                        var Curso = {
                            "codCurso": d.idCurso,
                            "nombreCurso": d.nombreCurso,
                            "descripcionCurso": d.descripcionCurso,
                        };
                        localStorage.setItem("Curso", JSON.stringify(Curso));
                        window.location.href = 'VistaCursoInicio.aspx';
                    });
                });
            }
        },
        error: function(r) {
            console.log(r.responseText);
        },
        failure: function(r) {
            console.log(r.responseText);
        }
    });
}