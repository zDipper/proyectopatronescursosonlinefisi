﻿using CapaEntidad;
using CapaLogica;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace CapaWebPresentacion
{
    /// <summary>
    /// Descripción breve de wsUsuario
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    [System.Web.Script.Services.ScriptService]
    public class wsUsuario : System.Web.Services.WebService
    {
        
        [WebMethod]
        public UsuarioBE ObtenerUsuarioCuenta(string correo, string password)
        {
            UsuarioBL usuarioBL = new UsuarioBL();
            return usuarioBL.ObtenerUsuarioCuenta(correo, password);
        }


        [WebMethod]
        public bool obtenerEmail(string correo)
        {
            UsuarioBL usuarioBL = new UsuarioBL();
            return usuarioBL.ExistCorreo(correo);
        }

        [WebMethod]
        public bool agregarUsuario(UsuarioBE objUsuario)
        {
            UsuarioBL usuarioBL = new UsuarioBL();
            String cod = objUsuario.DniUsuario;
            String mensaje = "";
            return usuarioBL.GuardarUsuario(objUsuario, out cod, out mensaje);
        }
    }
}
