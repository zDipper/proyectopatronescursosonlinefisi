﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="vistaCursoAlumno.aspx.cs" Inherits="CapaWebPresentacion.vistaCursoAlumno" %>

    <!DOCTYPE html>

    <html xmlns="http://www.w3.org/1999/xhtml">

    <head runat="server">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Cursos Online Cerseu</title>
        <link href="Content/bootstrap.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/solid.css" />
        <script src="https://use.fontawesome.com/releases/v5.0.7/js/all.js"></script>
        <script src="Scripts/umd/popper.min.js"></script>
        <script src="Scripts/jquery-3.4.1.min.js"></script>
        <script src="Scripts/bootstrap.min.js"></script>
        <link href="static/css/main.css" rel="stylesheet" />
    </head>

    <body>
        <header class="encabezado-Inicio">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand" href="Alumno.aspx"><img class="logo" src="static/img/cerseu.png" alt="logo Cerseu"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Categorias
                    </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="#">Programacion</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#">Ofimatica</a>
                            </div>
                        </li>
                        <li>
                            <form class="form-inline my-2 my-lg-0">
                                <input class="form-control mr-sm-2" id="inputBuscar" type="search" placeholder="Buscar" aria-label="Search" />
                                <button class="btn btn-outline-success my-2 my-sm-0" id="buscarAlumno" type="button"><i class="fas fa-search"></i></button>
                            </form>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="MisCursosAlumno.aspx">Mis Cursos</a>
                        </li>
                    </ul>
                    <div class="btn-group">
                        <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" data-display="static" aria-haspopup="true" aria-expanded="false">Configuracion</button>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg-left">
                            <button id="perfilAlumno" class="dropdown-item" type="button">Perfil</button>
                            <button class="dropdown-item" id="cerrarSesion" type="button">Cerrar sesion</button>
                        </div>
                    </div>
                </div>
            </nav>
        </header>

        <section class="vistaCurso container">
            <div id="TituloCurso">
            </div>
            <div id="matricula" class="text-right">
            </div>
            <div id="contenidoCurso">
            </div>
        </section>

        <section class="matricula">
            <div class="modal fade" id="Matricula" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Matriculate</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                        </div>
                        <div class="modal-body">
                            <form>
                                <div class="form-group pb-3">
                                    <input type="text" class="form-control" id="registroNombre" placeholder="N° VOUCHER">
                                </div>
                                <div class="form-group pb-3">
                                    <h5>Fecha de Pago</h5>
                                    <input type="date" class="form-control" id="registroApellidos">
                                </div>
                                <div class="form-group pb-3">
                                    <input type="text" readonly class="form-control" id="codCurso">
                                </div>
                                <div class="text-center pb-3">
                                    <button class="btn btn-info btn-block" type="submit">Matricularme</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <footer class="site-footer ">
            <div class="container clearfix ">
                <div class="footer-informacion ">
                    <h2>Sobre nosotros</h2>
                    <p>El Centro de Responsabilidad Social y Extensión Universitaria es el órgano de línea encargado de integrar a la Facultad de Ingeniería de Sistemas e Informática a la comunidad, a través de la organización de actividades de cultura general,
                        carácter profesional no escolarizado y prestación de servicios, con el apoyo de docentes y estudiantes.</p>
                    <p>La Extensión como vehículo ideal en la interacción recíproca Universidad-Sociedad, se erige como una labor social para la construcción de pertinencia social de la Institución en el sentido de cumplir con el deber educativo de formar
                        y desarrollar las potencialidades del estudiante, ofreciéndole para ello sensibilidad, competencias en los avances científicos y tecnológicos y escenarios propicios para su evolución y servicio a la comunidad.</p>
                </div>
                <div class="direccion ">
                    <div class="direccion-abajo ">
                        <p><i class="fas fa-map-marker-alt icono "></i>Ciudad Universitaria - Av. Germán Amézaga</p>
                        <p><i class="fas fa-phone icono "></i>Teléfono: 619 - 7000 Anexo 3615</p>
                        <p><i class="fas fa-at icono "></i>informatica.fisi@unmsm.edu.pe</p>
                    </div>
                </div>
            </div>
        </footer>

        <script src="static/js/main.js "></script>
        <script src="static/js/alumno.js"></script>
        <script src="static/js/capitulos.js"></script>
    </body>

    </html>