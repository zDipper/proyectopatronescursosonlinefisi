﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using CapaEntidad;
using CapaLogica;

namespace CapaPresentacionWeb
{
    /// <summary>
    /// Descripción breve de wsUsuario
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    [System.Web.Script.Services.ScriptService]
    public class wsUsuario : System.Web.Services.WebService
    {
        #region cursos
        [WebMethod]
        public int cursosTotales()
        {
            CursoBL cursoBL = new CursoBL();

            return cursoBL.ListarCursos().Count;
        }

        [WebMethod]
        public List<CursoBE> ObtenerCursosXNombre(string nombre)
        {
            CursoBL cursoBL = new CursoBL();
            return cursoBL.obtenerCursosXNombre(nombre);
        }

        [WebMethod]
        public List<CursoBE> listarCursosAlumno(string codAlumno)
        {
            AlumnoCursoBL alumnoCursoBL = new AlumnoCursoBL();
            CursoBL cursoBL = new CursoBL();
            List<AlumnoCursoBE> alumnoCursoBEs = alumnoCursoBL.listarCursosAlumno(codAlumno);
            List<CursoBE> cursoBEs = new List<CursoBE>();
            foreach (AlumnoCursoBE item in alumnoCursoBEs)
            {
                cursoBEs.Add(cursoBL.ObtenerCurso(item.idCurso));
            }
            return cursoBEs;
        }

        [WebMethod]
        public List<CapituloBE> listarCapitulosXCurso(string codCurso)
        {
            CapituloBL capituloBL = new CapituloBL();
            return capituloBL.listarCapitulosXCurso(codCurso);
        }

        [WebMethod]
        public bool agregarCurso(CursoBE objCurso)
        {
            CursoBL cursoBL = new CursoBL();
            String cod = objCurso.idCurso;
            String mensaje = "";
            return cursoBL.GuardarCurso(objCurso, out cod, out mensaje);
        }
        [WebMethod]
        public List<CursoBE> ObtenerCursos()
        {
            CursoBL cursoBL = new CursoBL();
            return cursoBL.ListarCursos();
        }

        [WebMethod]
        public CursoBE ObtenerCursoXCodigo(string codCurso)
        {
            CursoBL cursoBL = new CursoBL();
            return cursoBL.ObtenerCurso(codCurso);
        }

        [WebMethod]
        public bool actualizarCurso(CursoBE objCurso)
        {
            CursoBL cursoBL = new CursoBL();

            return cursoBL.ActualizarCurso(objCurso);
        }

        [WebMethod]
        public bool eliminarCurso(string codCurso)
        {
            CursoBL cursoBL = new CursoBL();
            return cursoBL.EliminarCurso(codCurso);
        }

        #endregion

        #region usuarios
        [WebMethod]
        public List<UsuarioBE> ObtenerUsuarios()
        {
            UsuarioBL objUsuarioBL = new UsuarioBL();
            return objUsuarioBL.ListarUsuarios();
        }

        [WebMethod]
        public bool actualizarUsuario(UsuarioBE objUsuario)
        {
            UsuarioBL objUsuarioBL = new UsuarioBL();

            return objUsuarioBL.ActualizarUsuario(objUsuario);
        }

        [WebMethod]
        public UsuarioBE ObtenerUsuarioCuenta(string correo, string password)
        {
            UsuarioBL usuarioBL = new UsuarioBL();
            return usuarioBL.ObtenerUsuarioCuenta(correo, password);
        }

        #endregion

        #region alumnos
        [WebMethod]
        public List<AlumnoBE> ObtenerAlumnos()
        {
            AlumnoBL objAlumnoBL = new AlumnoBL();
            return objAlumnoBL.ListarAlumnos();
        }

        [WebMethod]
        public AlumnoBE ObtenerAlumnoDni(string dni)
        {
            AlumnoBL alumnoBL = new AlumnoBL();
            return alumnoBL.ObtenerAlumnoDni(dni);
        }

        [WebMethod]
        public int alumnosTotales()
        {
            AlumnoBL alumnoBL = new AlumnoBL();

            return alumnoBL.ListarAlumnos().Count;
        }
        #endregion

        #region profesores
        [WebMethod]
        public List<ProfesorBE> ObtenerProfesores()
        {
            ProfesorBL objProfesorBL = new ProfesorBL();
            return objProfesorBL.ListarProfesors();
        }

        [WebMethod]
        public string ObtenerProfesor(string codProfesor)
        {
            ProfesorBL objProfesorBL = new ProfesorBL();
            UsuarioBL usuarioBL = new UsuarioBL();
            return usuarioBL.ObtenerUsuario(objProfesorBL.ObtenerProfesor(codProfesor).dniProfesor).NombresUsuario;
        }

        [WebMethod]
        public ProfesorBE ObtenerProfesorDni(string dni)
        {
            ProfesorBL ProfesorBL = new ProfesorBL();
            return ProfesorBL.ObtenerProfesorDni(dni);
        }

        [WebMethod]
        public int profesoresTotales()
        {
            ProfesorBL profesorBL = new ProfesorBL();

            return profesorBL.ListarProfesors().Count;
        }
        #endregion

        #region administradores
        [WebMethod]
        public AdministradorBE ObtenerAdministradorDni(string dni)
        {
            AdministradorBL administradorBL = new AdministradorBL();
            return administradorBL.ObtenerAdministradorDni(dni);
        }

        #endregion
    }
}
