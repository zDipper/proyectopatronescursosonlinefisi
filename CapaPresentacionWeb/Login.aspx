﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="CapaPresentacionWeb.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Login</title>

    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/solid.css"/>
    <script src="https://use.fontawesome.com/releases/v5.0.7/js/all.js"></script>
    <script src="Scripts/umd/popper.min.js"></script>
    <script src="Scripts/jquery-3.0.0.min.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
    <link href="static/css/StyleSheet1.css" rel="stylesheet" />
    <script src="static/js/JavaScript.js"></script>
</head>
<body>

    <div>
        <div class="modal-dialog text-center">
            <div class="col-sm-8 main-section">
                <div class="modal-content">
                    <div class="col-12 user-img">
                        <img src="static/img/avatar.png" />
                    </div>
                    <form class="col-12 " id="formulario">
                        <div class="form-group" id ="user-group">
                            <input type="text" class="form-control " id="emailInput" placeholder="email" />
                        </div>
                        <div class="form-group" id="password-group">
                            <input type="password" class="form-control " id="passwordInput" placeholder="password" />
                        </div>
                        <button type="submit" class="btn btn-primary"><i class="fas fa-sign-in-alt"></i>  Ingresar</button>
                    </form>
                    <div class="col-12 forgot">
                        <a href="#">recordar contraseña</a>
                    </div>
                    <div class="col-12 " id="alerta">
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>
</html>
