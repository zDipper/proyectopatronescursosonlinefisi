﻿$(function () {
   
    cargarUsuarioAdministrador();
    cerrarSesion();
    buscarCursoActualizar();
    agregarCurso();
    eliminarCurso();
});

$("#Cursos").click(function (e){
    llenarCursos();
})


function cargarUsuarioAdministrador() {
    var usuario = JSON.parse(localStorage.getItem("Usuario"));
    console.log(usuario);
    var Administrador = JSON.parse(localStorage.getItem("Administrador"));
    console.log(Administrador);
    console.log(usuario.sexoUsuario);
    if (usuario.sexoUsuario == "M") {
        $("#bienvenida").append("<h1>Bienvenido " + usuario.nombresUsuario + " !!</h1>");
    } else {
        $("#bienvenida").append("<h1>Bienvenida " + usuario.nombresUsuario + " !!</h1>");
    }

    llenarCursos();
    mostrarEstadisticasAlumnos();
}

function mostrarEstadisticasAlumnos() {
    $.ajax({
        method: "POST",
        url: "http://localhost:52345/wsUsuario.asmx/alumnosTotales",
        type: "json",
        contentType: "application/json; charset=utf-8",
        success: function (resp) {  
            $("#estadistica").empty();
            $("#estadistica").append('<div class="card text-white bg-success mb-3" style="max-width: 18rem;">' +
                '<div class="card-header">Alumnos</div>' +
                '<div class="card-body">' +
                '<h5 class="card-title">Cantidad de Alumnos</h5>' +
                '<p class="card-text">' + resp.d + '</p>' +
                '</div>' +
                '</div>');
            mostrarEstadisticasProfesores();
        },
        error: function (r) {
            alert(r.responseText);
        },
        failure: function (r) {
            alert(r.responseText);
        }
    })
}

function mostrarEstadisticasProfesores() {
    $.ajax({
        method: "POST",
        url: "http://localhost:52345/wsUsuario.asmx/profesoresTotales",
        type: "json",
        contentType: "application/json; charset=utf-8",
        success: function (resp) {
            $("#estadistica").append('<div class="card text-white bg-success mb-3" style="max-width: 18rem;">' +
                '<div class="card-header">Profesores</div>' +
                '<div class="card-body">' +
                '<h5 class="card-title">Cantidad de Profesores</h5>' +
                '<p class="card-text">' + resp.d + '</p>' +
                '</div>' +
                '</div>');
            mostrarEstadisticasCursos()
        },
        error: function (r) {
            alert(r.responseText);
        },
        failure: function (r) {
            alert(r.responseText);
        }
    })
}

function mostrarEstadisticasCursos() {
    $.ajax({
        method: "POST",
        url: "http://localhost:52345/wsUsuario.asmx/cursosTotales",
        type: "json",
        contentType: "application/json; charset=utf-8",
        success: function (resp) {
            $("#estadistica").append('<div class="card text-white bg-success mb-3" style="max-width: 18rem;">' +
                '<div class="card-header">Cursos</div>' +
                '<div class="card-body">' +
                '<h5 class="card-title">Cantidad de Cursos</h5>' +
                '<p class="card-text">' + resp.d + '</p>' +
                '</div>' +
                '</div>');
        },
        error: function (r) {
            alert(r.responseText);
        },
        failure: function (r) {
            alert(r.responseText);
        }
    })
}

function llenarCursos() {
    $.ajax({
        method: "POST",
        url: "http://localhost:52345/wsUsuario.asmx/ObtenerCursos",
        type: "json",
        contentType: "application/json; charset=utf-8",
        success: function (resp) {
            console.log(resp);
            $("#tbCuerpo").empty();
            $.each(resp.d, function (p, d) {
                $("#tbCuerpo").append(
                    "<tr>" +
                    "<td >" + d.idCurso + "</td>" +
                    "<td >" + d.nombreCurso + "</td>" +
                    "<td>" + d.codProfesor + "</td>" +
                    "</tr>"
                );
            });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(errorThrown);
        }
    });
}

function buscarCursoActualizar() {
    $("#btnActualizarCurso").click( function (e) {

        e.preventDefault();
        $.ajax({
            method: "POST",
            url: "http://localhost:52345/wsUsuario.asmx/ObtenerCursoXCodigo",
            data: JSON.stringify({ 'codCurso': $("#codigoCursoA").val() }),
            contentType: "application/json; charset=utf-8",
            success: function (resp) {
                console.log(resp.d);
                actualizarCurso(resp.d);
                $("#codigoCursoA").val("");
            },
        })
    })
}

function actualizarCurso(curso) {
    $("#codigoCursoActu").val(curso.idCurso);
    $("#nombreCursoInput").val(curso.nombreCurso);
    $("#comboProfesor").val(curso.codProfesor);
    $("#actualizaCursoSub").click(function (e) {
        var data = {
            objCurso: {
                idCurso: curso.idCurso,
                nombreCurso: $("#nombreCursoInput").val(),
                descripcionCurso: curso.descripcionCurso,
                codProfesor: $("#comboProfesor").val(),
            }
        }
        e.preventDefault();
        $.ajax({
            method: "POST",
            url: "http://localhost:52345/wsUsuario.asmx/actualizarCurso",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            success: function (resp) {
                console.log(resp);
                window.location.href = 'Administrador.aspx';
            },
        })
    })
}

function agregarCurso() {
    $("#btnAgregarCurso").click(function (e) {
        var data = {
            objCurso: {
                idCurso: $("#codigoCursoAg").val(),
                nombreCurso: $("#nombreCursoAg").val(),
                descripcionCurso: $("#descripcionCursoAg").val(),
                codProfesor: $("#codigoProfesorAg").val(),
            }
        }
        e.preventDefault();
        $.ajax({
            method: "POST",
            url: "http://localhost:52345/wsUsuario.asmx/agregarCurso",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            success: function (resp) {
                console.log(resp);
                window.location.href = 'Administrador.aspx';
            },
        })
    })

}

function eliminarCurso() {
    $("#btnEliminarCurso").click(function (e) {
        $.ajax({
            method: "POST",
            url: "http://localhost:52345/wsUsuario.asmx/eliminarCurso",
            data: JSON.stringify({ 'codCurso': $("#codigoCursoE").val() }),
            contentType: "application/json; charset=utf-8",
            success: function (resp) {
                console.log(resp.d);
                $("#codigoCursoE").val("");
                window.location.href = 'Administrador.aspx';
            },
        })
    })
    
}




function cerrarSesion() {
    $("#cerrarSesion").click(function (e) {
        localStorage.clear();
        window.location.href = 'login.aspx';
    })
}
