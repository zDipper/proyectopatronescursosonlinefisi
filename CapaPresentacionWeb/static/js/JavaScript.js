﻿$(function () {
    console.log("AEA");
    obtenerUsuario();

});

function obtenerUsuario() {
    $("#formulario").on("submit", function (e) {
        e.preventDefault();
        var data1 = $("#emailInput").val();
        var data2 = $("#passwordInput").val();
        var dni;
        $.ajax({
            method: "POST",
            url: "http://localhost:52345/wsUsuario.asmx/ObtenerUsuarioCuenta",
            data: "{ correo: '" + data1 + "', password: " + data2 + "}",
            DataType: "json",
            type: "json",
            contentType: "application/json; charset=utf-8",
            success: function (resp) {
                $.each(resp, function (i, item) {
                    dni = item.DniUsuario;
                    var Usuario = {
                        "dniUsuario": item.DniUsuario,
                        "nombresUsuario": item.NombresUsuario,
                        "apellidosUsuario": item.ApellidosUsuario,
                        "correoUsuario": item.CorreoUsuario,
                        "contrasenaUsuario": item.ContrasenaUsuario,
                        "sexoUsuario": item.SexoUsuario,
                        "telefonoUsuario": item.TelefonoUsuario,
                    };
                    localStorage.setItem("Usuario", JSON.stringify(Usuario));
                    if (dni == null) {
                        console.log(dni);
                        $("#alerta").empty();
                        $("#alerta").append('<div class= "alert alert-warning" role = "alert" >'+
                                                "correo o contraseña incorrecta"+
                                        '</div>');
                        $("#emailInput").val("");
                        $("#passwordInput").val("");
                    } else {
                        console.log(dni);
                        obtenerAlumno(dni);
                        //obtenerProfesor(dni);
                        //obtenerAdministrador(dni);
                    }

                });
            },
            error: function (r) {
                alert(r.responseText);
            },
            failure: function (r) {
                alert(r.responseText);
            }
        })

    });

}

function obtenerAlumno(dniAl) {
    var dniA = dniAl;
    var al;
    $.ajax({
        method: "POST",
        url: "http://localhost:52345/wsUsuario.asmx/ObtenerAlumnoDni",
        data: "{ dni: " + dniA + "}",
        DataType: "json",
        type: "json",
        contentType: "application/json; charset=utf-8",
        success: function (resp) {
            $.each(resp, function (i, item) {
                console.log(item);
                al = item.codAlumno;
                if (al !== null) {
                    var Alumno = {
                        "codAlumno": item.codAlumno,
                        "dniAlumno": item.dniAlumno,
                    };
                    localStorage.setItem("Alumno", JSON.stringify(Alumno));
                    window.location.href = 'Alumno.aspx';
                } else {
                    obtenerProfesor(dniA);
                }
                
            });
        },
        error: function (r) {
            console.log(r.responseText);
        },
        failure: function (r) {
            console.log(r.responseText);
        }
    })
    
}
function obtenerProfesor(dniProfe) {
    var dniP = dniProfe;
    var pro;
    $.ajax({
        method: "POST",
        url: "http://localhost:52345/wsUsuario.asmx/ObtenerProfesorDni",
        data: "{ dni: " + dniP + "}",
        DataType: "json",
        type: "json",
        contentType: "application/json; charset=utf-8",
        success: function (resp) {
            $.each(resp, function (i, item) {
                console.log(item);
                pro = item.codProfesor;
                if (pro !== null) {
                    var Profesor = {
                        "codProfesor": item.codProfesor,
                        "dniProfesor": item.dniProfesor,
                    };
                    localStorage.setItem("Profesor", JSON.stringify(Profesor));
                    window.location.href = 'Profesor.aspx';
                } else {
                    obtenerAdministrador(dniP);
                }
            });            
        },
        error: function (r) {
            console.log(r.responseText);
        },
        failure: function (r) {
            console.log(r.responseText);
        }
    })
}
function obtenerAdministrador(dniAdmin) {
    var dniA = dniAdmin;
    var ad;
    $.ajax({
        method: "POST",
        url: "http://localhost:52345/wsUsuario.asmx/ObtenerAdministradorDni",
        data: "{ dni: " + dniA + "}",
        DataType: "json",
        type: "json",
        contentType: "application/json; charset=utf-8",
        success: function (resp) {
            $.each(resp, function (i, item) {
                console.log(item);
                ad = item.codAdministrador;
                if (ad !== null) {
                    var Administrador = {
                        "codAdministrador": item.codAdministrador,
                        "dniAdministrador": item.dniAdministrador,
                    };
                    localStorage.setItem("Administrador", JSON.stringify(Administrador));
                    window.location.href = 'Administrador.aspx';
                } else {
                    $("#alerta").empty();
                    $("#alerta").append('<div class= "alert alert-warning" role = "alert" >' +
                        "no tiene un rol asignado" +
                        '</div>');
                    $("#emailInput").val("");
                    $("#passwordInput").val("");
                }
            });
        },
        error: function (r) {
            console.log(r.responseText);
        },
        failure: function (r) {
            console.log(r.responseText);
        }
    })
}
