﻿$(function () {
    cargarUsuarioAlumno();
    listarCurso();
    buscarCurso();
    cerrarSesion();
});

function cargarUsuarioAlumno() {
    var usuario = JSON.parse(localStorage.getItem("Usuario"));
    console.log(usuario);
    var alumno = JSON.parse(localStorage.getItem("Alumno"));
    console.log(alumno);
    console.log(usuario.sexoUsuario);
    $("#bienvenida").append("<h3 class='text-white'> Hola de nuevo, " + usuario.nombresUsuario + " !!</h3>" +
            "<h4  class='text-white'>   ¿Preparado para estudiar uno de tus cursos?</h4>");
    mostrarCursosAlumno(alumno.codAlumno);
    editarPerfil(alumno, usuario);
}

function mostrarCapitulosCurso(idCurso) {
    console.log(idCurso);
    $.ajax({
        method: "POST",
        url: "http://localhost:52345/wsUsuario.asmx/listarCapitulosXCurso",
        data: JSON.stringify({ 'idCurso': idCurso }),
        type: "json",
        contentType: "application/json; charset=utf-8",
        success: function (resp) {
            console.log("tengo sueño")
            $("#principal").addClass('hidden')
          
        },
        error: function (r) {
            console.log(r.responseText);
        },
        failure: function (r) {
            console.log(r.responseText);
        }
    })
}

function buscarCurso() {
    $("#buscarCursos").click( function (e) {
        //e.preventDefault();
        var curso = $("#cursosBuscar").val();
        $.ajax({
            method: "POST",
            url: "http://localhost:52345/wsUsuario.asmx/ObtenerCursosXNombre",
            data: JSON.stringify({ 'nombre': curso }),
            type: "json",
            contentType: "application/json; charset=utf-8",
            success: function (resp) {
                console.log(resp);
                $("#main-content").empty();
                $("#main-content").append(
                    "<div style='font-size: 22px; color: #b6ff00; margin-left: 1em; padding-top: 1em;'>Resultado de búsqueda</div>"
                )
                
                $.each(resp.d, function (p, d) {
                    console.log(d);
                    $("#main-content").append(
                        "<div class='glovo cursoItem' idCurso='" + d.idCurso + "'>" +
                        "<img class='img' src='static/img/javascript.jpg' />" +
                        "<div class='card-content'>" +
                        "<div id='tituloBusqueda' class='title'>" + d.nombreCurso + "</div>" +
                        "<div id='descripcionBusqueda' class='description'>" + d.descripcionCurso + "</div>" +
                        "</div>" +
                        "</div >");
                   //
                   // $("#descripcionBusqueda").append(d.descripcionCurso)
                      
                });
                $("#cursosBuscar").val("");

                $(".cursoItem").click(function (e) {
                    console.log($(this).attr('idCurso'));
                });
               // mostrarCapitulosCurso(idCurso);
            },
            error: function (r) {
                console.log(r.responseText);
            },
            failure: function (r) {
                console.log(r.responseText);
            }
        })
    });
}

function listarCurso() {
        var curso = "";
    $.ajax({
        method: "POST",
        url: "http://localhost:52345/wsUsuario.asmx/ObtenerCursosXNombre",
        data: JSON.stringify({ 'nombre': curso }),
        type: "json",
        contentType: "application/json; charset=utf-8",
        success: function (resp) {
                console.log(resp);
                $("#main-content").empty();
                $("#main-content").append(
                    "<div style='font-size: 22px; color: #b6ff00; margin-left: 1em; padding-top: 1em;'>Cursos Disponibles</div>"
                )

                $.each(resp.d, function (p, d) {
                    console.log(d);
                    $("#main-content").append(
                        "<div class='glovo cursoItem' idCurso='" + d.idCurso + "'>" +
                        "<img class='img' src='static/img/javascript.jpg' />" +
                        "<div class='card-content ' >" +
                        "<div id='tituloBusqueda' class='title'>" + d.nombreCurso + "</div>" +
                        "<div id='descripcionBusqueda' class='description'>" + d.descripcionCurso + "</div>" +
                        "</div>" +
                        "</div >");

                });
                $("#cursosBuscar").val("");

                $(".cursoItem").click(function (e) {
                    console.log($(this).attr('idCurso'));
                });
           // mostrarCapitulosCurso(resp.d.idCurso);

            },
            error: function (r) {
                console.log(r.responseText);
            },
            failure: function (r) {
                console.log(r.responseText);
            }
        })
   
}

function mostrarCursosAlumno(codAlumno) {
    console.log(codAlumno);
    $.ajax({
        method: "POST",
        url: "http://localhost:52345/wsUsuario.asmx/listarCursosAlumno",
        data: JSON.stringify({ 'codAlumno': codAlumno }),
        type: "json",
        contentType: "application/json; charset=utf-8",
        success: function (resp) {
            console.log(resp);
            $("#cursosAlumno").empty();
            $.each(resp.d, function (p, d) {
                console.log(d.nombreCurso);
                $("#cursosAlumno").append(
                    "<div class=' row row-cols-1 row-cols-md-2'>"+
                        "<div class='col mb-4'>"+
                            "<div class='glovo card'>"+
                                "<!--<img src='...' class='card-img-top' alt='...'>-->"+
                                "<div class='card-body'>"+
                                    "<h5 class='card-title'>" + d.nombreCurso+"</h5>"+
                    "<p class='card-text'>" + d.descripcionCurso + "</p>" +
                    
                                "</div>"+
                            "</div>"+
                        "</div>"+
                    "</div>"
                );
            });
            $("#cursosBuscar").val("");
        },
        error: function (r) {
            console.log(r.responseText);
        },
        failure: function (r) {
            console.log(r.responseText);
        }
    })
}

function editarPerfil(alumno,usuario) {
    $("#mostrarPerfil").click(function (e) {
        console.log(alumno);
        console.log(usuario);
        $("#codigoAlumno").val(alumno.codAlumno);
        $("#nombreAlumnoInput").val(usuario.nombresUsuario);
        $("#apellidoAlumnoInput").val(usuario.apellidosUsuario);
        $("#correoAlumnoInput").val(usuario.correoUsuario);
        $("#passwordAlumnoInput").val(usuario.contrasenaUsuario);
        $("#telefonoAlumnoInput").val(usuario.telefonoUsuario);

        $("#perfilForm").on("submit", function (e) {
            e.preventDefault();
            var data = {
                objUsuario: {
                    DniUsuario: usuario.dniUsuario,
                    NombresUsuario: $("#nombreAlumnoInput").val(),
                    ApellidosUsuario: $("#apellidoAlumnoInput").val(),
                    CorreoUsuario: $("#correoAlumnoInput").val(),
                    ContrasenaUsuario: $("#passwordAlumnoInput").val(),
                    SexoUsuario: usuario.sexoUsuario,
                    TelefonoUsuario: $("#telefonoAlumnoInput").val(),
                }
            }
            usuario.nombresUsuario = data.objUsuario.NombresUsuario;
            usuario.apellidosUsuario = data.objUsuario.ApellidosUsuario;
            usuario.correoUsuario = data.objUsuario.CorreoUsuario;
            usuario.contrasenaUsuario = data.objUsuario.ContrasenaUsuario;
            usuario.telefonoUsuario = data.objUsuario.TelefonoUsuario;
            $.ajax({
                method: "POST",
                url: "http://localhost:52345/wsUsuario.asmx/actualizarUsuario",
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json"
            }).done(function (info) {
                //Respuesta del servidor
                console.log(info);
                $("#perfil").modal("hide");
            })
        });
    })

}

function cerrarSesion() {
    $("#cerrarSesion").click(function (e) {
        localStorage.clear();
        window.location.href = 'login.aspx';
    })
 }




