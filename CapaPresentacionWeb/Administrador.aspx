﻿.
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Administrador.aspx.cs" Inherits="CapaPresentacionWeb.Administrador" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Administrador</title>
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <script src="Scripts/umd/popper.min.js"></script>
    <script src="Scripts/jquery-3.0.0.min.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
    <script src="static/js/administrador.js"></script>
</head>
<body>
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="administrador.aspx">Cursos Online Fisi</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item active">
                    <a class="nav-link" href="administrador.aspx">Inicio <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item ">
                    <a class="nav-link " id="Cursos" href="#" data-toggle="modal" data-target="#administrarCurso" >Cursos<span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item ">
                    <a class="nav-link" id="administrarProfesores" href="#">Profesores<span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item ">
                    <a class="nav-link" id="administrarAlumnos" href="#">Alumnos<span class="sr-only">(current)</span></a>
                    </li>
                </ul>
            </div>
            <form class="form-inline my-2 my-lg-0">
                <button class="btn btn-outline-danger my-2 my-sm-0" id="cerrarSesion" type="button">Cerrar sesion</button>
            </form>
        </nav>
    
        <div id="bienvenida">

        </div>

        <div class="col-12">
            <h1>Estadisticas :</h1>
            <div class="card-deck" id="estadistica">
                
            </div>
        </div>

        <div class="modal fade " id="administrarCurso">
            <div class="modal-dialog modal-lg ">
                <div class="modal-content">
                    <!-- header -->
                    <div class="modal-header">
                        <h5 class="modal-title">Cursos</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <!-- body -->
                    <div class="modal-body">
                        <table class="table table-dark">
                          <thead>
                            <tr>
                              <th scope="col">IDCURSO</th>
                              <th scope="col">NOMBRECURSO</th>
                              <th scope="col">CODPROFESOR</th>
                            </tr>
                          </thead>
                          <tbody id="tbCuerpo">
                          </tbody>
                        </table>
                    </div>
                    <!-- footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-warning" data-toggle="modal"  data-target="#actualizarCurso" data-dismiss="modal" >Actualizar</button>
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#agregarCurso" data-dismiss="modal">Agregar Curso</button>
                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#eliminarCurso" data-dismiss="modal">Borrar Curso</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="actualizarCurso">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- header -->
                    <div class="modal-header">
                        <h5 class="modal-title">Curso a Actualizar</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <!-- body -->
                    <div class="modal-body">
                        <form id="buscarCursoForm" method="post">
                            <div class="form-group">
                                <label for="codigoCursoA">codigo Curso</label>
                                <input type="text" class="form-control" id="codigoCursoA" />
                            </div>
                            
                            <button type="submit"  class="btn btn-primary"data-toggle="modal" id="btnActualizarCurso" data-target="#formActualizarCurso" data-dismiss="modal">Buscar</button>
                        </form>
                    </div>
                    <!-- footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                    </div>
                </div>
            </div>
        </div>

        
        <div class="modal fade" id="formActualizarCurso">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <!-- header -->
                    <div class="modal-header">
                        <h5 class="modal-title">Actualizar Curso</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <!-- body -->
                    <div class="modal-body">
                        <form id="actualizarCursoForm" method="post">
                            <div class="form-group">
                                <label for="codigoCursoActu">codigo Curso</label>
                                <input type="text" class="form-control" readonly="readonly" id="codigoCursoActu" />
                            </div>
                            <div class="form-group">
                                <label for="nombreCursoInput">nombre</label>
                                <input type="text" class="form-control" id="nombreCursoInput" />
                            </div>
                            <div class="form-group">
                                <label for="comboProfesor">CodigoProfesor</label>
                                <input type="text" class="form-control" id="comboProfesor" />
                            </div>

                            
                            <button type="submit" id="actualizaCursoSub"  class="btn btn-primary" data-dismiss="modal">Actualizar</button>
                        </form>
                    </div>
                    <!-- footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="agregarCurso">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- header -->
                    <div class="modal-header">
                        <h5 class="modal-title">Curso a Agregar</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <!-- body -->
                    <div class="modal-body">
                        <form id="AgregarCursoForm" method="post">
                            <div class="form-group">
                                <label for="codigoCursoAg">codigo Curso</label>
                                <input type="text" class="form-control" id="codigoCursoAg" />
                            </div>
                            <div class="form-group">
                                <label for="nombreCursoAg">Nombre Curso</label>
                                <input type="text" class="form-control" id="nombreCursoAg" />
                            </div>
                            <div class="form-group">
                                <label for="descripcionCursoAg">Descripcion Curso</label>
                                <input type="text" class="form-control" id="descripcionCursoAg" />
                            </div>
                            <div class="form-group">
                                <label for="codigoProfesorAg">Profesor Curso</label>
                                <input type="text" class="form-control" id="codigoProfesorAg" />
                            </div>
                            
                            <button type="button"  class="btn btn-primary" id="btnAgregarCurso" >Agregar</button>
                        </form>
                    </div>
                    <!-- footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="eliminarCurso">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- header -->
                    <div class="modal-header">
                        <h5 class="modal-title">Curso a Borrar</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <!-- body -->
                    <div class="modal-body">
                        <form id="eliminarCursoForm" method="post">
                            <div class="form-group">
                                <label for="codigoCursoE">codigo Curso</label>
                                <input type="text" class="form-control" id="codigoCursoE" />
                            </div>
                            
                            <button type="submit"  class="btn btn-primary" id="btnEliminarCurso" data-dismiss="modal">Eliminar</button>
                        </form>
                    </div>
                    <!-- footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                    </div>
                </div>
            </div>
        </div>

    
    </div>
        
</body>
</html>
