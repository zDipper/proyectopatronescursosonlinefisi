﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Alumno.aspx.cs" Inherits="CapaPresentacionWeb.Alumno" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Alumno</title>    
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="static/css/Alumno.css"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/solid.css"/>
    <script src="https://use.fontawesome.com/releases/v5.0.7/js/all.js"></script>
    <script src="Scripts/umd/popper.min.js"></script>
    <script src="Scripts/jquery-3.0.0.min.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
    <script src="static/js/alumno.js"></script>
   
</head>
<body>

    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light " style="background-color: #0a3458;">
            <a class="navbar-brand text-white text-lg-left  font-weight-bold" href="Alumno.aspx" style="font-size: 30px">Cursos Online Fisi</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            
            <div class="collapse navbar-collapse" id="navbarNav">
                <form class="form-inline" id="busca">
                    <input class="form-control" id="cursosBuscar" type="text" placeholder="Busca un curso"/>
                    <button class="btn btn-outline-success my-2 my-sm-0" id="buscarCursos" type="button" ><i class="fas fa-search"></i></button>
                </form>
                <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                    <li class="nav-item active">
                    <a class="nav-link text-white" href="Alumno.aspx">Inicio <span class="sr-only">(current)</span></a>
                    </li>
                    <!--<li class="nav-item ">
                    <a class="nav-link " id="mostrarCursos" type="button" href="#" data-toggle="modal" data-target="#cursos">Curso<span class="sr-only">(current)</span></a>
                    </li>-->
                    <li class="nav-item ">
                    <a class="nav-link text-white" id="mostrarPerfil"  href="#" data-toggle="modal" data-target="#perfil">Perfil<span class="sr-only">(current)</span></a>
                    </li>
                </ul>
                <form class="form-inline my-2 my-lg-0">
                    <button class="btn btn-outline-danger my-2 my-sm-0" id="cerrarSesion" type="button">Cerrar sesion</button>
                </form>
            </div>
        </nav>

        <div id="principal">
        <div id="bienvenida">
            <img id="estudia" class='img' src='static/img/fondo.jpg' />"
            
        </div>
        <div  id="cursosAlumno">
              
        </div>

        <section id="main-content">
            <!--div style='font-size: 22px; color: #b6ff00; margin-left: 1em; padding-top: 1em;'>Cursos</!--div-->
            
          
        </section>



     
        
        <div class="modal fade" id="perfil">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <!-- header -->
                    <div class="modal-header">
                        <h5 class="modal-title">Perfil</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <!-- body -->
                    <div class="modal-body">
                        <form id="perfilForm" method="post">
                            <div class="form-group">
                                <label for="codigoAlumno">codigo Alumno</label>
                                <input type="text" class="form-control" readonly="readonly" id="codigoAlumno" />
                            </div>
                            <div class="form-group">
                                <label for="nombreAlumnoInput">nombre</label>
                                <input type="text" class="form-control" id="nombreAlumnoInput" />
                            </div>
                            <div class="form-group">
                                <label for="apellidoAlumnoInput">Apellidos</label>
                                <input type="text" class="form-control" id="apellidoAlumnoInput" />
                            </div>
                            <div class="form-group">
                                <label for="correoAlumnoInput">Correo</label>
                                <input type="text" class="form-control" id="correoAlumnoInput" />
                            </div>
                            <div class="form-group">
                                <label for="passwordAlumnoInput">Contraseña</label>
                                <input type="password" class="form-control" id="passwordAlumnoInput" />
                            </div>
                            <div class="form-group">
                                <label for="telefonoAlumnoInput">Telefono</label>
                                <input type="text" class="form-control" id="telefonoAlumnoInput" />
                            </div>
                            <button type="submit"  class="btn btn-primary" >Actualizar</button>
                        </form>
                    </div>
                    <!-- footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                    </div>
            </div>
                </div>
        </div>
    </div>
   </div>
</body>
</html>
