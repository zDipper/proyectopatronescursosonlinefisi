﻿using CapaDeDatos;
using CapaEntidad;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaLogica
{
    public class UsuarioBL
    {
        #region Variables

        private UnidadTrabajo unidadTrabajo = new UnidadTrabajo();

        #endregion

        public UsuarioBE ObtenerUsuario(string dniUsuario)
        {
            try
            {
                return unidadTrabajo.UsuarioRepositorio.GetByID(dniUsuario);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<UsuarioBE> ListarUsuarios()
        {
            try
            {
                return unidadTrabajo.UsuarioRepositorio.Get();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                unidadTrabajo = null;
            }
        }

        public bool GuardarUsuario(UsuarioBE objUsuario, out string dni, out string mensaje)
        {
            try
            {
                if (objUsuario.CorreoUsuario == "")
                {
                    dni = "";
                    mensaje = "El correo del usuario no puede estar vacio.";
                    return false;
                }
                if (objUsuario.ContrasenaUsuario == "")
                {
                    dni = "";
                    mensaje = "La contraseña del usuario no puede estar vacia.";
                    return false;
                }

                if (unidadTrabajo.UsuarioRepositorio.Insert(objUsuario))
                {
                    unidadTrabajo.Save();
                    dni = objUsuario.DniUsuario;
                    mensaje = "Se registro al usuario";
                    return true;
                }
                else
                {
                    dni = "";
                    mensaje = "Ocurrio un error al guardar al usuario.";
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarUsuario(UsuarioBE objUsuario)
        {
            try
            {
                if (unidadTrabajo.UsuarioRepositorio.Update(objUsuario))
                {
                    unidadTrabajo.Save();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool EliminarUsuario(string dni)
        {
            try
            {
                if (unidadTrabajo.UsuarioRepositorio.Delete(dni))
                {
                    unidadTrabajo.Save();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public UsuarioBE ObtenerUsuarioCuenta(string correo, string password)
        {
            UsuarioDA usuarioDA = new UsuarioDA();
            return usuarioDA.ObtenerUsuarioCuenta(correo, password);
        }

        public bool ExistCorreo(string correo)
        {
            UsuarioDA usuarioDA = new UsuarioDA();
            return usuarioDA.ExistEmail(correo);
        }
    }
}
