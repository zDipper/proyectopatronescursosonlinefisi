﻿using CapaDeDatos;
using CapaEntidad;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaLogica
{
    public class ProfesorBL
    {
        #region Variables
        private UnidadTrabajo unidadTrabajo = new UnidadTrabajo();

        #endregion

        public ProfesorBE ObtenerProfesor(string cod)
        {

            try
            {
                return unidadTrabajo.ProfesorRepositorio.GetByID(cod);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ProfesorBE> ListarProfesors()
        {

            try
            {
                return unidadTrabajo.ProfesorRepositorio.Get();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                unidadTrabajo = null;
            }
        }

        public bool GuardarProfesor(ProfesorBE objProfesor, out string cod, out string mensaje)
        {

            try
            {
                if (unidadTrabajo.ProfesorRepositorio.Insert(objProfesor))
                {
                    mensaje = "Se registro al Profesor.";
                    cod = objProfesor.codProfesor;
                    unidadTrabajo.Save();
                    return true;
                }
                else
                {
                    cod = "";
                    mensaje = "Ocurrio un error al guardar al Profesor.";
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarProfesor(ProfesorBE objProfesor)
        {

            try
            {
                if (unidadTrabajo.ProfesorRepositorio.Update(objProfesor))
                {
                    unidadTrabajo.Save();
                    return true;
                }
                else return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool EliminarProfesor(string cod)
        {

            try
            {
                if (unidadTrabajo.ProfesorRepositorio.Delete(cod))
                {
                    unidadTrabajo.Save();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ProfesorBE ObtenerProfesorDni(string dni)
        {
            ProfesorDA profesorDA = new ProfesorDA();
            return profesorDA.ObtenerProfesorDni(dni);
        }
    }
}
