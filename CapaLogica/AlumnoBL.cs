﻿using CapaDeDatos;
using CapaEntidad;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaLogica
{
    public class AlumnoBL
    {
        #region Variables

        private UnidadTrabajo unidadTrabajo = new UnidadTrabajo();

        #endregion

        public AlumnoBE ObtenerAlumno(string cod)
        {

            try
            {
                return unidadTrabajo.AlumnoRepositorio.GetByID(cod);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<AlumnoBE> ListarAlumnos()
        {

            try
            {
                return unidadTrabajo.AlumnoRepositorio.Get();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                unidadTrabajo = null;
            }
        }

        public bool GuardarAlumno(AlumnoBE objAlumno, out string cod, out string mensaje)
        {

            try
            {
                
                if (unidadTrabajo.AlumnoRepositorio.Insert(objAlumno))
                {
                    mensaje = "Se registro al Alumno.";
                    cod = objAlumno.codAlumno;
                    unidadTrabajo.Save();
                    return true;
                }
                else
                {
                    cod = "";
                    mensaje = "Ocurrio un error al guardar al Alumno.";
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarAlumno(AlumnoBE objAlumno)
        {

            try
            {
                if (unidadTrabajo.AlumnoRepositorio.Update(objAlumno))
                {
                    unidadTrabajo.Save();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool EliminarAlumno(string cod)
        {
            try
            {
                if (unidadTrabajo.AlumnoRepositorio.Delete(cod))
                {
                    unidadTrabajo.Save();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        public AlumnoBE ObtenerAlumnoDni(string dni)
        {
            AlumnoDA alumnoDA = new AlumnoDA();
            return alumnoDA.ObtenerAlumnoDni(dni);
        }

        public string ObtenerUCodigoAlumno()
        {
            AlumnoDA alumnoDA = new AlumnoDA();
            return alumnoDA.ObtenerUltimoCodigoAlumno();
        }

    }

}
