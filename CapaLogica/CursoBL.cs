﻿using CapaDeDatos;
using CapaEntidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaLogica
{
    public class CursoBL
    {
        #region variables

        private UnidadTrabajo unidadTrabajo = new UnidadTrabajo();

        #endregion

        public CursoBE ObtenerCurso(string cod)
        {

            try
            {
                return unidadTrabajo.CursoRepositorio.GetByID(cod);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CursoBE> ListarCursos()
        {

            try
            {
                return unidadTrabajo.CursoRepositorio.Get();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                unidadTrabajo = null;
            }
        }

        public bool GuardarCurso(CursoBE objCurso, out string cod, out string mensaje)
        {
            try
            {
                if (unidadTrabajo.CursoRepositorio.Insert(objCurso))
                {
                    unidadTrabajo.Save();
                    cod = objCurso.idCurso;
                    mensaje = "Se registro al Curso";
                    return true;
                }
                else
                {
                    cod = "";
                    mensaje = "Ocurrio un error al guardar al Curso.";
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarCurso(CursoBE objCurso)
        {

            try
            {
                if (unidadTrabajo.CursoRepositorio.Update(objCurso))
                {
                    unidadTrabajo.Save();
                    return true;
                }
                else
                {
                    return false;
                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool EliminarCurso(string cod)
        {

            try
            {
                if (unidadTrabajo.CursoRepositorio.Delete(cod))
                {
                    unidadTrabajo.Save();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CursoBE> obtenerCursosXNombre(string nombre)
        {
            CursoDA cursoDA = new CursoDA();
            try
            {
                return cursoDA.ObtenerCursosXNombre(nombre);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CursoBE> ObtenerCursosProfe(string codProfe)
        {
            CursoDA cursoDA = new CursoDA();
            try
            {
                return cursoDA.ObtenerCursosProfesor(codProfe);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
