﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaDeDatos;
using CapaEntidad;

namespace CapaLogica
{
    public class CapituloBL
    {
        #region Variables

        private UnidadTrabajo unidadTrabajo = new UnidadTrabajo();

        #endregion

        public CapituloBE ObtenerCapitulo(int cod)
        {
            try
            {
                return unidadTrabajo.CapituloRepositorio.GetByID(cod);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CapituloBE> ListarCapitulos()
        {

            try
            {
                return unidadTrabajo.CapituloRepositorio.Get();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                unidadTrabajo = null;
            }
        }

        public bool GuardarCapitulo(CapituloBE objCapitulo, out int cod, out string mensaje)
        {

            try
            {

                if (unidadTrabajo.CapituloRepositorio.Insert(objCapitulo))
                {
                    mensaje = "Se registro al Capitulo.";
                    cod = objCapitulo.idCapitulo;
                    unidadTrabajo.Save();
                    return true;
                }
                else
                {
                    cod = 0;
                    mensaje = "Ocurrio un error al guardar al Capitulo.";
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarCapitulo(CapituloBE objCapitulo)
        {

            try
            {
                if (unidadTrabajo.CapituloRepositorio.Update(objCapitulo))
                {
                    unidadTrabajo.Save();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool EliminarCapitulo(int cod)
        {

            try
            {
                if (unidadTrabajo.CapituloRepositorio.Delete(cod))
                {
                    unidadTrabajo.Save();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CapituloBE> listarCapitulosXCurso (string codCurso)
        {
            CapituloDA capituloDA = new CapituloDA();
            try
            {
                return capituloDA.listarCapitulosXCurso(codCurso);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
