﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaEntidad;
using CapaDeDatos;

namespace CapaLogica
{
    public class AlumnoCursoBL
    {
        #region Variables

        private UnidadTrabajo unidadTrabajo = new UnidadTrabajo();

        #endregion

        public AlumnoCursoBE ObtenerAlumnoCurso(string id)
        {

            try
            {
                return unidadTrabajo.AlumnoCursoRepositorio.GetByID(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<AlumnoCursoBE> ListarAlumnoCursos()
        {

            try
            {
                return unidadTrabajo.AlumnoCursoRepositorio.Get();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                unidadTrabajo = null;
            }
        }

        public bool GuardarAlumnoCurso(AlumnoCursoBE objAlumnoCurso, out string id, out string mensaje)
        {

            try
            {

                if (unidadTrabajo.AlumnoCursoRepositorio.Insert(objAlumnoCurso))
                {
                    mensaje = "Se registro al AlumnoCurso.";
                    id = objAlumnoCurso.idCurso;
                    unidadTrabajo.Save();
                    return true;
                }
                else
                {
                    id = "";
                    mensaje = "Ocurrio un error al guardar al AlumnoCurso.";
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarAlumnoCurso(AlumnoCursoBE objAlumnoCurso)
        {

            try
            {
                if (unidadTrabajo.AlumnoCursoRepositorio.Update(objAlumnoCurso))
                {
                    unidadTrabajo.Save();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool EliminarAlumnoCurso(string cod)
        {

            try
            {
                if (unidadTrabajo.AlumnoCursoRepositorio.Delete(cod))
                {
                    unidadTrabajo.Save();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<AlumnoCursoBE> listarCursosAlumno(string codAlumno)
        {
            AlumnoCursoDA alumnoCursoDA = new AlumnoCursoDA();
            try
            {
                return alumnoCursoDA.listarCursosAlumno(codAlumno);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
