﻿using CapaDeDatos;
using CapaEntidad;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaLogica
{
    public class AdministradorBL
    {
        #region Variables

        UnidadTrabajo unidadTrabajo = new UnidadTrabajo();

        #endregion

        public AdministradorBE ObtenerAdministrador(string cod)
        {

            try
            {
                return unidadTrabajo.AdministradorRepositorio.GetByID(cod);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<AdministradorBE> ListarAdministradores()
        {

            try
            {
                return unidadTrabajo.AdministradorRepositorio.Get();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                unidadTrabajo = null;
            }
        }

        public bool GuardarAdministrador(AdministradorBE objAdministrador, out string cod, out string mensaje)
        {

            try
            {

                if (unidadTrabajo.AdministradorRepositorio.Insert(objAdministrador))
                {
                    mensaje = "Se registro al Administrador.";
                    cod = objAdministrador.codAdministrador;
                    unidadTrabajo.Save();
                    return true;
                }
                else
                {
                    cod = "";
                    mensaje = "Ocurrio un error al guardar al Administrador.";
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarAdministrador(AdministradorBE objAdministrador)
        {

            try
            {
                if (unidadTrabajo.AdministradorRepositorio.Update(objAdministrador))
                {
                    unidadTrabajo.Save();
                    return true;
                }
                else return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool EliminarAdministrador(string cod)
        {

            try
            {
                if (unidadTrabajo.AdministradorRepositorio.Delete(cod))
                {
                    unidadTrabajo.Save();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public AdministradorBE ObtenerAdministradorDni(string dni)
        {
            AdministradorDA administradorBE = new AdministradorDA();
            return administradorBE.ObtenerAdministradorDni(dni);
        }
    }
}
